extends Spatial

func open():
	yield(get_tree().create_timer(randf() / 2), "timeout")
	$AnimationPlayer.play("open")
