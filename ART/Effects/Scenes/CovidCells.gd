extends Spatial

func _ready():
	$AnimationPlayer.play("default")

func _on_AnimationPlayer_animation_finished(anim_name):
	$AnimationPlayer.play(anim_name)
