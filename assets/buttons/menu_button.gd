tool
extends TextureButton

export(String) var scene_to_load
export(String) var text setget set_text

func _on_play_button_pressed():
	Transition.transition(scene_to_load)
	
func set_text(t):
	text = t
	$Label.text = t
