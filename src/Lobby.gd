extends Node

signal session_joined(name)
signal session_already_registered(infos)
signal invalid_session_name(name)
signal player_added(id, player_options)
signal game_started()
signal games_obtained(games)
signal game_ready
signal player_removed(player)
signal server_connected()
signal server_disconnected()
signal session_invalid_password(name, password)
signal session_closed()

var is_server = false
var game_is_ready = false

var server_ip
var server_port

const MAX_PLAYER = 4

var character_packed = {
	"Old Karl" : preload("res://src/characters/OldKarlCharacter.tscn"),
	"Punky" : preload("res://src/characters/PunkyCharacter.tscn"),
	"Gangsta" : preload("res://src/characters/GangstaCharacter.tscn"),
	"Doc" : preload("res://src/characters/DocCharacter.tscn"),
}

var player
var player_ready = 0
var player_options = {}
var players_options = {}
var session_name
var id
var server_id

onready var rtc_client = $WebRTCClient
var rtc_mp
	
func remove_player_options(u_id):
	if players_options.has(u_id):
		var p_options = players_options[u_id]
		players_options.erase(u_id)
		emit_signal("player_removed", u_id, p_options)
	
	
func start(p_options, s_name, as_server = false, session_password = ''):
	randomize()
	rtc_mp = WebRTCMultiplayer.new()
	if as_server:
		rtc_client.register_session(p_options, s_name, session_password)
		
	else :
		rtc_client.join_session(p_options, s_name, session_password)
		
	print("restarted")
	is_server = as_server
	
	player_options = p_options
	
#	player_options.u_id = udp_punch.id
	
	player = null
	player_ready = 0
	players_options = {}
	game_is_ready = false
	
func restart():
	quit()
	
	yield(get_tree().create_timer(2.0), "timeout")
	
	rtc_client.start()
	
func request_games():
	rtc_client.get_infos()
		
func start_game():
	rpc("emit_game_started")
	
remotesync func emit_game_started():
	emit_signal("game_started")
	
func _player_connected(player_id):
	print("player connected ", player_id)
	rpc_id(player_id, "add_new_player", get_tree().get_network_unique_id(), player_options)
	
func _player_disconnected(player_id):
	print("player disconnected ", player_id)
	if players_options.has(player_id):
		emit_signal("player_removed", player_id, players_options[player_id])
		players_options.erase(player_id)

func _connected_ok():
	create_player(id, player_options)
	
func _server_connected():
	emit_signal("server_connected")

func _server_disconnected():
	get_tree().set_network_peer(null)
	emit_signal("server_disconnected")

func _connected_fail():
	get_tree().set_network_peer(null)
	
func quit():
	rtc_client.stop()
	rtc_mp = null
	get_tree().network_peer = null
	
func build_player_from_options(player_id, p_options):
	var pl = character_packed[p_options.character].instance()
	pl.u_id = player_id
	pl.set_name(str(player_id))
	pl.player_name = p_options.player_name
	pl.set_network_master(player_id)
	return pl
	
	
func create_player(player_id, p_options):
	players_options[player_id] = p_options
	emit_signal("player_added", player_id, p_options)
	
	
remotesync func set_game_ready():
	emit_signal("game_ready")

remote func add_new_player(u_id, name):
	create_player(u_id, name)
	

func _on_WebRTCClient_already_registered_session(infos):
	emit_signal("session_already_registered", infos)

func _on_WebRTCClient_offer_received(player_id, offer):
	if rtc_mp.has_peer(mp_id(player_id)):
		rtc_mp.get_peer(mp_id(player_id)).connection.set_remote_description("offer", offer)


func _on_WebRTCClient_answer_received(player_id, answer):
	if rtc_mp.has_peer(mp_id(player_id)):
		rtc_mp.get_peer(mp_id(player_id)).connection.set_remote_description("answer", answer)


func _on_WebRTCClient_candidate_received(player_id, mid, index, sdp):
	if rtc_mp.has_peer(mp_id(player_id)):
		rtc_mp.get_peer(mp_id(player_id)).connection.add_ice_candidate(mid, index, sdp)


func _on_WebRTCClient_offer_created(type, data, player_id):
	if rtc_mp.has_peer(mp_id(player_id)):
		rtc_mp.get_peer(mp_id(player_id)).connection.set_local_description(type, data)


func _on_WebRTCClient_infos_obtained(infos):
	id = infos.id
	emit_signal("games_obtained", infos.sessions)


func _on_WebRTCClient_invalid_session_password(name, password):
	emit_signal("session_invalid_password", name, password)

func _on_WebRTCClient_session_registered(infos):
	_on_join(infos)

	
func _on_join(infos):
	server_id = infos.server_id
	rtc_mp.transfer_mode = NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE
	rtc_mp.initialize(mp_id(id))
	get_tree().network_peer = rtc_mp
	if not get_tree().is_connected("network_peer_connected", self, "_player_connected"):
		var _c = get_tree().connect("network_peer_connected", self, "_player_connected")
		_c = get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		_c = get_tree().connect("connected_to_server", self, "_connected_ok")
		_c = get_tree().connect("connection_failed", self, "_connected_fail")
		_c = get_tree().connect("server_disconnected", self, "_server_disconnected")

	emit_signal("session_joined", infos)
	create_player(mp_id(id), player_options)
	
func mp_id(player_id):
	return 1 if player_id == server_id else player_id


func _on_WebRTCClient_new_player(player_id, peer):
	rtc_mp.add_peer(peer, mp_id(player_id))

	if mp_id(id) > rtc_mp.get_unique_id():
		print("create offer")
		peer.create_offer()


func _on_WebRTCClient_session_joined(infos):
	_on_join(infos)


func _on_WebRTCClient_session_closed():
	emit_signal("session_closed") 
