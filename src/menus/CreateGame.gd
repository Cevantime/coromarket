extends Control

signal back_requested()
#signal new_game_created(name, password)
signal new_game_creation_requested(name, password)

onready var error_label = $VBoxContainer/Panel/MarginContainer/VBoxContainer/ErrorLabel

func _ready():
	var _c = Lobby.connect("session_already_registered", self, "_on_Lobby_session_already_registered")

func _on_BackButton_pressed():
	emit_signal("back_requested")


func _on_CreateButton_pressed():
	var game_name = $VBoxContainer/Panel/MarginContainer/VBoxContainer/GameNameLineEdit.text.to_lower()
	
	var password = $VBoxContainer/Panel/MarginContainer/VBoxContainer/GamePasswordLineEdit.text.to_lower()
	if not game_name :
		error_label.text = "Game name should not be empty"
		error_label.show()
	
	else:
		emit_signal("new_game_creation_requested", game_name, password)
		Lobby.start({
			"player_name": PlayerVariables.get_data(PlayerVariables.PLAYER_NAME),
			"character": PlayerVariables.get_data(PlayerVariables.PLAYER_CHARACTER)
		}, game_name, true, password)
		
	
func _on_Lobby_session_already_registered(_infos):
	error_label.text = "This game name already exists"
	error_label.show()

func _on_Lobbysession_joined(_infos):
	print("infos")
	Transition.transition("res://src/menus/GameLobby3D.tscn")
