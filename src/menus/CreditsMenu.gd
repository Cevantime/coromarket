extends Control

func _ready():
	$MarginContainer/VBoxContainer/back_button.grab_focus()

func _on_back_button_pressed():
	Transition.transition("res://src/menus/MainMenu.tscn", "curtain", 2.0)
