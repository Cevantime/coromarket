extends Control


func _ready():
	$MarginContainer/VBoxContainer/HBoxContainer/back_button.grab_focus()

func _on_back_button_pressed():
	Transition.transition("res://src/menus/MainMenu.tscn")


func _on_StartButton_pressed():
	Transition.transition("res://src/menus/Tutorial.tscn")
