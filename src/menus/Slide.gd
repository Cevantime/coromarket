tool
extends NinePatchRect

signal previous_requested
signal next_requested

export(Texture) var texture_1 setget set_texture_1
export(Texture) var texture_2 setget set_texture_2
export(Texture) var texture_3 setget set_texture_3
export(Texture) var texture_4 setget set_texture_4

export(String, MULTILINE) var text setget set_text

func play_out():
	$AnimationPlayer.play("out")
	
func play_out_backwards():
	$AnimationPlayer.play_backwards("out")
	
func play_in():
	$AnimationPlayer.play("in")
	
func play_in_backwards():
	$AnimationPlayer.play_backwards("in")

func set_tex(tex, idx):
	var rect = get_node("HBoxContainer/TextureRect%s" % idx)
	rect.texture = tex
	rect.visible = tex != null

func set_texture_1(tex):
	set_tex(tex,1)
	texture_1 = tex
	
func set_texture_2(tex):
	set_tex(tex,2)
	texture_2 = tex

func set_texture_3(tex):
	set_tex(tex,3)
	texture_3 = tex

func set_texture_4(tex):
	set_tex(tex,4)
	texture_4 = tex
	
func set_text(t):
	text = t
	$RichTextLabel.bbcode_text = t


func _on_PreviousButton_pressed():
	emit_signal("previous_requested")


func _on_NextButton_pressed():
	emit_signal("next_requested")
