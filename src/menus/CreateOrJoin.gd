extends Control

signal join_choosed
signal create_choosed
signal back_requested

onready var character_option = $ChooseYourCharacter/MarginContainer/VBoxContainer2/VBoxContainer/CharacterOption

var items = {}

func _ready():
	var i = 0
	for c in Lobby.character_packed:
		character_option.add_item(c)
		items[i] = c
		i += 1
	if PlayerVariables.PLAYER_CHARACTER:
		var name = PlayerVariables.get_data(PlayerVariables.PLAYER_CHARACTER)
		for j in range(i):
			if str(name) == items[j]:
				character_option.select(j)
				break


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_CharacterOption_item_selected(id):
	PlayerVariables.set_data(PlayerVariables.PLAYER_CHARACTER, items[id])
	PlayerVariables.save_data()

func _on_JoinGameButton_pressed():
	emit_signal("join_choosed")
	


func _on_CreateGameButton_pressed():
	emit_signal("create_choosed")


func _on_BackButton_pressed():
	emit_signal("back_requested")


func _on_CreateOrJoin_visibility_changed():
	if visible:
		$ChooseYourCharacter/MarginContainer/VBoxContainer2/VBoxContainer/CharacterOption.grab_focus()
