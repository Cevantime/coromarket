extends Control

var scene_path_to_load
var music = preload("res://assets/music/MENU.ogg")

func _ready():
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_EXPAND, Vector2(1040, 600))
	Transition.play_music("res://assets/music/MENU.ogg")
	yield(get_tree().create_timer(0.5), "timeout")
	$AnimationPlayer.play("start")
	Lobby.connect("session_joined", self, "_on_Lobby_session_joined")
	$StartContainer/HBoxContainer/Buttons/StartButton.grab_focus()

func _on_Lobby_session_joined(_data):
	Transition.transition("res://src/menus/GameLobby3D.tscn")


func _on_StartButton_pressed():
#	Transition.transition("res://src/menus/GameMenu.tscn")
	$AnimationPlayer.play_backwards("start")
	if PlayerVariables.get_data(PlayerVariables.PLAYER_NAME):
		$AnimationPlayer2.play("create_or_join_in")
	else:
		$AnimationPlayer2.play("name_in")
	


func _on_PlayerNameContainer_validated():
	$AnimationPlayer.play("name_out")
	Transition.transition("res://src/menus/Tutorial.tscn")


func _on_CreateOrJoin_join_choosed():
	$AnimationPlayer.play("create_or_join_out")
	$AnimationPlayer2.play("list_games_in")
	$ListGames.start_request_games()


func _on_ListGames_back_requested():
	$ListGames.stop_request_games()
	$AnimationPlayer.play_backwards("list_games_in")
	$AnimationPlayer2.play_backwards("create_or_join_out")


func _on_CreateGame_back_requested():
	$AnimationPlayer.play_backwards("create_game_in")
	$AnimationPlayer2.play_backwards("create_or_join_out")


func _on_CreateGame_new_game_created(_name, _password):
	pass # Replace with function body.


func _on_CreateOrJoin_create_choosed():
	$AnimationPlayer.play("create_or_join_out")
#	yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer2.play("create_game_in")


func _on_CreateOrJoin_back_requested():
	$AnimationPlayer.play_backwards("create_or_join_in")
	$AnimationPlayer2.play("start")


func _on_ListGames_create_game_requested():
	$ListGames.stop_request_games()
	$AnimationPlayer.play_backwards("list_games_in")
	$AnimationPlayer2.play("create_game_in")


func _on_StartContainer_visibility_changed():
	$StartContainer/HBoxContainer/Buttons/StartButton.grab_focus()
