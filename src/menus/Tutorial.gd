extends Control

var current_index = 0

func _ready():
	for i in range(get_child_count()):
		var s = get_child(i)
		s.connect("previous_requested", self, "_on_previous")
		s.connect("next_requested", self, "_on_next")
	
	slide_to(0)
		
func slide_to(i):
	if i == current_index:
		return
	i = clamp(i, 0, get_child_count() - 1)
	var children = get_children()
	var current = children[current_index]
	var next = children[i]
	
	
	if i > current_index:
		current.play_out()
		next.play_in()
	elif i < current_index:
		current.play_in_backwards()
		next.play_out_backwards()
		
	current_index = i
	
func _on_previous():
	slide_to(current_index - 1)
	
func _on_next():
	slide_to(current_index + 1)

func _on_FinishButton_pressed():
	if PlayerVariables.get_data(PlayerVariables.SCREEN_RESOLUTION_ID):
		OS.window_size = PlayerVariables.get_screen_resolution()
	Transition.transition("res://src/menus/MainMenu.tscn")
