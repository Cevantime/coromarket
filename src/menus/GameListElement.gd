extends MarginContainer

signal game_requested(game)
signal game_selected(game)

var game setget set_game

func set_game(g):
	game = g
	$HBoxContainer/GameNameLabel.text = game.name
	$HBoxContainer/HBoxContainer/GamePlayersLabel.text = "%s/4" % game.members
	if g.password:
		$HBoxContainer/HBoxContainer/Locked.show()
	
func _gui_input(event):
	if event is InputEventMouseButton and event.is_pressed():
		if event.doubleclick:
			emit_signal("game_requested", game)
		else:
			selected(true)
			emit_signal("game_selected", game)

func selected(selected):
	if selected:
		$ColorRect.color.a = 0.2
	else: 
		$ColorRect.color.a = 0.0
