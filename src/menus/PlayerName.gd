extends Control

signal validated()

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_OkNameButton_pressed():
	var name = $Panel/VBoxContainer/HBoxContainer/LineEdit.text
	var msg_label = $Panel/VBoxContainer/MsgLabel
	if name.length() > 20: 
		msg_label.text = "The name is too long !"
		yield(get_tree().create_timer(2.0), "timeout")
		msg_label.text = "Please enter your name"
	else :
		PlayerVariables.set_data(PlayerVariables.PLAYER_NAME, name)
		PlayerVariables.save_data()
		emit_signal("validated")
