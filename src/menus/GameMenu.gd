extends Control

onready var character_option = $VBoxContainer/HBoxContainer2/VBoxContainer2/CharacterOption
onready var player_name_edit = $VBoxContainer/HBoxContainer2/VBoxContainer/NameLineEdit
onready var game_name_edit = $VBoxContainer/MarginContainer/VBoxContainer/GameNameEdit
onready var join_game_button = $VBoxContainer/HBoxContainer/JoinGameButton
onready var new_game_button = $VBoxContainer/HBoxContainer/NewGameButton

var game_packed_scene = preload("res://src/main_scenes/NetworkedScene.tscn")
var items = {}

func _ready():
	var i = 0
	Lobby.connect("invalid_session_name", self, "_on_invalid_session_name")
	Lobby.connect("session_already_registered", self, "_on_session_already_registered")
	Lobby.connect("session_joined", self, "_on_session_joined")
	for c in Lobby.character_packed:
		character_option.add_item(c)
		items[i] = c
		i += 1

func join_game(as_server = false) :
	var player_name = player_name_edit.text
	var session_name = game_name_edit.text
	session_name = session_name.to_lower()
	if ! player_name or ! session_name:
		message("Please enter a name and a session name.")
		return
	
	message("Connecting...")
	Lobby.start({ 
		"player_name": player_name, 
		"character" : items[character_option.selected] 
	}, session_name, as_server)

func message(msg):
	var msg_label = $VBoxContainer/MarginContainer/VBoxContainer/MessageLabel
	msg_label.text = msg
	msg_label.show()
	
	
func _on_invalid_session_name(name):
	join_game_button.disabled = false
	message("The game doesn't exist.")
	
func _on_session_already_registered(name):
	new_game_button.disabled = false
	message("The game already exists.")

func _on_session_joined(infos):
	change_to_game()

func _on_JoinGameButton_pressed():
	join_game_button.disabled = true
	join_game(false)

func _on_NewGameButton_pressed():
	new_game_button.disabled = true
	join_game(true)

func change_to_game():
	get_tree().change_scene("res://src/main_scenes/NetworkedScene.tscn")

func _on_BackButton_pressed():
	get_tree().change_scene("res://src/menus/MainMenu.tscn")
