extends Control

signal back_requested()
signal create_game_requested()
signal join_game_requested(game)

var game_selected

onready var game_list = $MarginContainer/VBoxContainer/Panel/MarginContainer/VBoxContainer/ScrollContainer/VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	var _c = Lobby.connect("games_obtained", self, "_on_Lobby_games_obtained")

func start_request_games():
	refresh_games()
	$TimerRequestGames.start()

func stop_request_games():
	$TimerRequestGames.stop()

func refresh_games():
	Lobby.request_games()
	

func _on_Lobby_games_obtained(games):
	for game_list_element in game_list.get_children():
		game_list_element.free()
	for g in games:
		var game_list_element = preload("res://src/menus/GameListElement.tscn").instance()
		game_list_element.set_game(g)
		game_list_element.connect("game_requested", self, "_on_game_requested")
		game_list_element.connect("game_selected", self, "_on_game_selected")
		if game_selected:
			game_list_element.selected(game_list_element.game.name == game_selected.name)
		game_list.add_child(game_list_element)

func join_game(game, password = ""):
	stop_request_games()
	Lobby.start({
		"player_name": PlayerVariables.get_data(PlayerVariables.PLAYER_NAME),
		"character": PlayerVariables.get_data(PlayerVariables.PLAYER_CHARACTER),
	}, game.name, false, password)
	emit_signal("join_game_requested", game)
	

func _on_JoinButton_pressed():
	_on_game_requested(game_selected)


func _on_CreateButton_pressed():
	emit_signal("create_game_requested")

func _on_TimerRequestGames_timeout():
	refresh_games()
	
func _on_game_requested(game):
	if not game:
		return
	if game.password != "":
		game_selected = game
		$MarginContainer/VBoxContainer/Panel/PanelPassword.show()
	else:
		join_game(game)

func _on_game_selected(game):
	game_selected = game
	for game_list_element in game_list.get_children():
		game_list_element.selected(game_list_element.game == game)
	

func _on_BackButton_pressed():
	emit_signal("back_requested")


func _on_ValidatePasswordButton_pressed():
	var password = $MarginContainer/VBoxContainer/Panel/PanelPassword/VBoxContainer/PasswordLineEdit.text.to_lower()
	if password == game_selected.password:
		join_game(game_selected, password)
	else: 
		$MarginContainer/VBoxContainer/Panel/PanelPassword/VBoxContainer/ErrorPasswordLabel.show()

func _on_CancelButton_pressed():
	$MarginContainer/VBoxContainer/Panel/PanelPassword.hide()
