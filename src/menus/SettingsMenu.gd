extends Control

onready var resolution_option = $MarginContainer/VBoxContainer/Control/Video/VBoxContainer/CenterContainer/VBoxContainer/OptionButton


var play_sounds = false

const RESOLUTIONS = [
	"1920x1080",
	"1680x1050",
	"1280x720",
	"1040x600",
	"1024x768",
	"960x640",
	"800x600"
]

func _ready():
	$MarginContainer/VBoxContainer/CenterContainer2/back_button.grab_focus()
	var screen_size = OS.get_screen_size()
	for r in RESOLUTIONS:
		var size = get_resolution_size(r)
		
		if screen_size.x < size.x:
			continue
		
		resolution_option.add_item(r)
	var PV = PlayerVariables
	if PV.get_data(PV.SCREEN_RESOLUTION_ID):
		resolution_option.selected = PV.get_data(PV.SCREEN_RESOLUTION_ID)
	
	$MarginContainer/VBoxContainer/Control/Audio/VBoxContainer/CenterContainer/VBoxContainer/HSlider.value = db2linear(PV.get_bus_volume(PV.MASTER_VOLUME)) 
	$MarginContainer/VBoxContainer/Control/Audio/VBoxContainer/CenterContainer2/VBoxContainer/HSlider2.value = db2linear(PV.get_bus_volume(PV.FX_VOLUME))
	$MarginContainer/VBoxContainer/Control/Audio/VBoxContainer/CenterContainer3/VBoxContainer/HSlider3.value = db2linear(PV.get_bus_volume(PV.MUSIC_VOLUME))
	
	play_sounds = true
	
	$MarginContainer/VBoxContainer/Control/General/VBoxContainer/CenterContainer2/CenterContainer/NameLineEdit.text = PV.get_data(PV.PLAYER_NAME) if PV.has_data(PV.PLAYER_NAME) else ''
	$MarginContainer/VBoxContainer/Control/Video/VBoxContainer/CenterContainer2/VBoxContainer2/OptionButton.pressed = PV.get_data(PV.FULLSCREEN) == "1"
	
func _on_back_button_pressed():
	PlayerVariables.save_data()
	Transition.transition("res://src/menus/MainMenu.tscn")

func set_setting(name, value):
	PlayerVariables.set_data(name, value)
	PlayerVariables.save_data()

func get_resolution_size(res) -> Vector2:
	var r = res.split("x")
	return Vector2(r[0], r[1])

func _on_OptionButton_item_selected(id):
	var size = get_resolution_size(RESOLUTIONS[id])
	PlayerVariables.set_data(PlayerVariables.SCREEN_RESOLUTION_ID, id)
	PlayerVariables.set_data(PlayerVariables.SCREEN_RESOLUTION_X, size.x)
	PlayerVariables.set_data(PlayerVariables.SCREEN_RESOLUTION_Y, size.y)
	PlayerVariables.save_data()
	
	OS.window_size = size


func _on_OptionButton_toggled(button_pressed):
	OS.window_fullscreen = button_pressed
	set_setting(PlayerVariables.FULLSCREEN, "1" if button_pressed else "0")


	
func get_bus_value(bus_name):
	return AudioServer.get_bus_volume_db(AudioServer.get_bus_index(bus_name))
	
func set_bus_value(bus_name, value):
	var db_value = linear2db(value)
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index(bus_name), db_value)
	return db_value

func _on_HSlider_value_changed(value):
	value = set_bus_value("Master", value)
	value = "-inf" if  value == -INF else value 
	PlayerVariables.set_data(PlayerVariables.MASTER_VOLUME, value)


func _on_HSlider2_value_changed(value):
	if play_sounds:
		$AudioStreamPlayer.play()
	value = set_bus_value("FX", value)
	value = "-inf" if value == -INF else value 
	PlayerVariables.set_data(PlayerVariables.FX_VOLUME, value)



func _on_HSlider3_value_changed(value):
	value = set_bus_value("Music", value)
	value = "-inf" if  value == -INF else value 
	PlayerVariables.set_data(PlayerVariables.MUSIC_VOLUME, value)



func _on_NameLineEdit_text_changed(new_text):
	PlayerVariables.set_data(PlayerVariables.PLAYER_NAME, new_text)
