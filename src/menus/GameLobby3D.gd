extends Spatial

var resource_loader
var players = {}


# Called when the node enters the scene tree for the first time.
func _ready():
	$StartCanvasLayer/Control/StartButton.visible = Lobby.is_server
	var _c = Lobby.connect("game_started", self, "_on_game_started")
	_c = Lobby.connect("player_added", self, "_on_player_added")
	_c = Lobby.connect("player_removed", self, "_on_player_removed")
	_c = Lobby.connect("session_closed", self, "_on_session_closed")
	
	for id in Lobby.players_options.keys():
		var options = Lobby.players_options[id]
		add_player(id, options)
	
	resource_loader = ResourceLoader.load_interactive("res://src/main_scenes/NetworkedScene.tscn")

func _process(_delta):
	var err = resource_loader.poll()

	if err == ERR_FILE_EOF:
		set_process(false)
	
	
func _on_game_started():
	if not resource_loader.get_resource():
		resource_loader.wait()
	Transition.transition_to(resource_loader.get_resource())


func add_player(id, player_options):
	
	var player = Lobby.build_player_from_options(id, player_options)
	player.set_process(false)
	if player.has_node("NotifyNetwork"):
		player.get_node("NotifyNetwork").queue_free()
	player.play("IdleWaiting")
	
	for i in range(4):
		var pos = get_node("CharacterPositions/PositionPlayer%s" % i)
		if pos.get_child_count() == 0:
			pos.add_child(player)
			break
	
	players[id] = player
	
	var ids = players.keys()
	
	ids.sort()
	
	var i = 0
	
	for id in ids:
		players[id].set_color_edited(i)
		i += 1
	
func _on_player_added(id, player_options):
	add_player(id, player_options)
	
func _on_player_removed(id, _player):
	print("player lost ", id)
	for i in range(4):
		var pos = get_node("CharacterPositions/PositionPlayer%s" % i)
		if pos.get_child_count() > 0 and pos.get_child(0).u_id == id:
			pos.get_child(0).call_deferred("queue_free")
			break
	

func _on_StartButton_pressed():
	Lobby.start_game()

	
func _on_BackButton_pressed():
	_quit()
	
func _on_session_closed():
	_quit()
	
func _quit():
	Lobby.restart()
	
	get_tree().network_peer = null
	
	Transition.transition("res://src/menus/MainMenu.tscn")
