extends Spatial

export(Array, PackedScene) var product_scenes

var product = null

var index_product = 0
var Product = preload("res://src/Product.gd")

func _ready():
#	yield(get_tree(),"idle_frame")
	var _c = $SpawnerStuff/Area.connect("body_entered", self, "_on_Area_body_entered")
	_c = $SpawnerStuff/Area.connect("body_exited", self, "_on_Area_body_exited")
	
remotesync func pop_product(product_infos):
	product = ProductFactory.build_product_from_infos(product_infos)
	product.transform = $SpawnerStuff/PopPoint.global_transform
	product.state = Product.STATES.FLOATING
	index_product += 1
	add_product_to_root(product)
	
remotesync func unset_product():
	product = null

func add_product_to_root(_product):
	var scene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)
	scene.call_deferred("add_child", _product)
	
func _on_Area_body_entered(body):
	if body.has_method("push_stand"):
		body.push_stand(self)

func _on_Area_body_exited(body):
	if body.has_method("remove_stand"):
		body.remove_stand(self)

