extends CharacterState

var shopping_kart

func _enter_state(_previous_state):
	if not shopping_kart:
		shopping_kart = referer.shopping_karts_in_range_for_deposit[0]
	if not is_instance_valid(referer.product):
		referer.run()
		return
	
func _physics_process(_delta):
	
	var dir = shopping_kart.get_node("CollectArea").global_transform.origin - referer.global_transform.origin
	referer.angle = Vector2(dir.x, dir.z).angle()
	referer.velocity = Vector3.ZERO
	var self_basis = referer.global_transform.basis.x
	var self_angle = Vector2(self_basis.x, self_basis.z).angle()
	var diff_angle = referer.angle - self_angle;
	
	if abs(diff_angle) < 0.075:
		set_physics_process(false)
		if get_tree().get_network_peer():
			referer.rpc("play", "deposit")
		else:
			referer.play("deposit")
			
		yield(referer.get_body().get_node("AnimationPlayer"), "animation_finished")
		
		if is_instance_valid(referer.product):
			if get_tree().get_network_peer():
				referer.rpc("play", "take")
			else:
				referer.play("take")
			referer.change_state("TakeProduct")
		else :
			referer.product = null
			referer.change_state("Running")
		
		
func _exit_state(_next_state):
	shopping_kart = null

