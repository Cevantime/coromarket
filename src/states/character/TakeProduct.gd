extends MultipleStateMachine

func _supports(node):
	return node is Character

func _enter_state(_previous_state):
	referer.velocity = Vector3.ZERO
	set_enabled(false)
	yield(get_tree().create_timer(0.3), "timeout")
	set_enabled(true)
	._enter_state(_previous_state)
