extends CharacterState

var ShoppingCart = preload("res://src/ShoppingKart.gd")


func _enter_state(_previous_state):
	if referer.shopping_kart.state == ShoppingCart.STATES.GROOVE_TO_CHECKOUT:
		referer.change_state("PutObjects")
		return
	referer.shopping_kart.CONTROLLED = true
	
func _process(_delta):
	if Input.is_action_just_pressed("leave_shopping_kart" + referer.get_device_str()):
		referer.leave_shopping_kart()

func _physics_process(_delta):
	referer.global_transform = referer.shopping_kart.get_character_transform()
	referer.global_transform.origin.y = referer.original_y
	
