extends CharacterState


func _physics_process(delta):
	if not referer.shopping_kart:
		referer.change_state("Running")
	else :
		var target_pos = referer.shopping_kart.get_character_transform()
		var diff_pos = target_pos.origin - referer.global_transform.origin
		diff_pos.y = 0.0
		if diff_pos.length() < referer.SPEED * delta:
			referer.velocity =  diff_pos
			referer.change_state("RotatingToPushPosition")
		else:
			referer.velocity = diff_pos.normalized() * referer.SPEED
			referer.angle = Vector2(diff_pos.x, diff_pos.z).angle()
