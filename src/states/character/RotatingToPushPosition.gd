extends CharacterState

	
func _physics_process(_delta):
	referer.global_transform.origin = referer.shopping_kart.get_character_transform().origin
	referer.global_transform.origin.y = referer.original_y
	var dir = referer.shopping_kart.global_transform.basis.x 
	referer.angle = Vector2(dir.x, dir.z).angle()
	
	var self_basis = referer.global_transform.basis.x
	var diff_angle = Vector2(dir.x, dir.z).angle_to(Vector2(self_basis.x, self_basis.z));
	
	if abs(diff_angle) < 0.01:
		referer.change_state("Pushing")
		if get_tree().get_network_peer():
			referer.rpc("play", "take_shopping_kart")
		else:
			referer.play("take_shopping_kart")
		yield(get_tree().create_timer(0.3), "timeout")
		referer.shopping_kart.CONTROLLED = true
		
