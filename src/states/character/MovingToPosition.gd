extends CharacterState

func _physics_process(delta):
	var diff_pos = referer.target_position.origin - referer.global_transform.origin
	diff_pos.y = 0.0
	if diff_pos.length() < referer.SPEED * delta:
		referer.velocity =  diff_pos
		referer.change_state("RotatingToPosition")
	else:
		referer.velocity = diff_pos.normalized() * referer.SPEED
		referer.angle = Vector2(diff_pos.x, diff_pos.z).angle()
