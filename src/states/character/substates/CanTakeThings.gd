extends CharacterState

func _enter_state(_previous_state):
	if get_tree().network_peer:
		referer.rpc("play", "running")
	else :
		referer.play("running")
	if referer.product != null :
		referer.drop_product()
	if referer.shopping_kart:
		referer.shopping_kart.leaved_by_player()

func _process(_delta):
	if not referer.CONTROLLED:
		return
		
	if Input.is_action_just_pressed("take" + referer.get_device_str()) and not referer.product and referer.selected_stand:
		if get_tree().network_peer and not is_network_master():
			referer.rpc("request_take_stand_product", get_tree().get_network_unique_id(), referer.selected_stand)
		else:
			referer.take_stand_product(referer.selected_stand)
	elif Input.is_action_just_pressed("pickup" + referer.get_device_str()) and not referer.product and referer.pickable_product:
		if get_tree().network_peer and not is_network_master():
			referer.rpc("request_pickup_product", get_tree().get_network_unique_id(), referer.pickable_product)
		else:
			referer.pickup_product()
	elif Input.is_action_just_pressed("take_shopping_kart" + referer.get_device_str()) and not referer.product and referer.shopping_kart_in_range :
		if get_tree().network_peer and not is_network_master():
			referer.rpc("take_shopping_kart", get_tree().get_network_unique_id(), referer.shopping_kart_in_range[0])
		else:
			referer.take_shopping_kart()
		referer.take_shopping_kart()
	elif Input.is_action_just_pressed("strike" + referer.get_device_str()):
		referer.strike()
			
