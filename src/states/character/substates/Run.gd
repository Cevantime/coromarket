extends CharacterState

func _physics_process(_delta):
	var velocity = Vector3()
	
	if referer.CONTROLLED:
		velocity.x = Input.get_action_strength("right" + referer.get_device_str()) - Input.get_action_strength("left" + referer.get_device_str())
		velocity.z = Input.get_action_strength("down" + referer.get_device_str()) - Input.get_action_strength("up" + referer.get_device_str())
	
	if velocity.length() > 0.0:
		referer.dir = velocity.normalized()
		referer.angle = Vector2(velocity.x, velocity.z).angle()
	else :
		referer.angle = null
		
	referer.velocity = velocity.normalized() * referer.SPEED

	referer.global_transform.origin.y = referer.original_y
	referer.velocity.y = 0
