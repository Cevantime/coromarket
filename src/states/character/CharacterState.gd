extends StateMachine
class_name CharacterState

func _supports(node):
	return node is Character
