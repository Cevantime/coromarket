extends CharacterState

	
func _physics_process(_delta):
	referer.global_transform.origin = referer.target_position.origin
	var dir = referer.target_position.global_transform.basis.x 
	referer.angle = Vector2(dir.x, dir.z).angle()
	
	var self_basis = referer.global_transform.basis.x
	var diff_angle = Vector2(dir.x, dir.z).angle_to(Vector2(self_basis.x, self_basis.z));
	
	if abs(diff_angle) < 0.03:
		referer.change_state("Running")
		
