extends CharacterState

var mash_cumulate = 0
var locked = false


func _enter_state(_previous_state):
	if get_tree().network_peer:
		referer.rpc("play", "start_putting_objects")
	else:
		referer.play("start_putting_objects")
	referer.shopping_kart.CONTROLLED = false
	referer.get_body().get_node("AnimationPlayer").connect("animation_finished", self, "_on_animation_finished")

func _exit_state(_next_state):
	referer.get_body().get_node("AnimationPlayer").disconnect("animation_finished", self, "_on_animation_finished")

func _physics_process(_delta):
	referer.global_transform = referer.shopping_kart.get_character_transform()
	referer.global_transform.origin.y = referer.original_y
	
func _process(_delta):
	
	if Input.is_action_just_pressed("put_object" + referer.get_device_str()):
		mash_cumulate += 1
	
	if mash_cumulate >= referer.MASH_NUMBER_TO_PUT_OBJECT and not locked:
		locked = true
		mash_cumulate = 0
		referer.put_next_object()

func _on_animation_finished(_animation):
	
	locked = false
