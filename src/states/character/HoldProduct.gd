extends CharacterState

func _process(delta):
	if not is_instance_valid(referer.product):
		referer.run()
	if referer.CONTROLLED:
		if Input.is_action_just_released("launch" + referer.get_device_str()) :
			if referer.power > referer.MIN_POWER:
				referer.launch()
				referer.power = 0
				referer.change_state("Locked")
				yield(get_tree().create_timer(0.3),"timeout")
				referer.change_state("Running")
			else: 
				referer.power = 0
		elif referer.power <= 0.05 and Input.is_action_pressed("deposit" + referer.get_device_str()) and not referer.shopping_karts_in_range_for_deposit.empty():
			referer.deposit_product()
		elif Input.is_action_pressed("launch" + referer.get_device_str()):
			referer.power = clamp(referer.power + referer.POWER_SPEED * delta, 0, referer.power_max)
			
