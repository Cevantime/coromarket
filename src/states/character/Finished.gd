extends CharacterState

func _enter_state(_previous_state):
	referer.shopping_kart.CONTROLLED = true
	
func _physics_process(_delta):
	referer.global_transform = referer.shopping_kart.get_character_transform()
