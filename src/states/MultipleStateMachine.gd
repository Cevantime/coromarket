extends StateMachine
class_name MultipleStateMachine

func _supports(_node):
	return true

func _enter_state(previous):
	for s in get_children():
		if not s._supports(referer):
			continue
		s.set_enabled(true)
		s._enter_state(previous)
		
func _exit_state(next):
	for s in get_children():
		if not s._supports(referer):
			continue
		s.set_enabled(false)
		s._exit_state(next)
