extends Node
class_name StateMachine

var referer 
var enabled = false

func _enter_tree():
	enabled = not get_parent().has_method("_enter_state") and _supports(get_parent())
	if enabled:
		referer = get_parent()
	elif 'referer' in get_parent():
		referer = get_parent().referer
	
		
func _ready():
	if enabled:
		set_enabled(true)
		_enter_state(null)
	else :
		set_enabled(false)

func set_enabled(_enabled):
	enabled = _enabled
	set_physics_process(_enabled)
	set_process(_enabled)
	if referer.has_signal("integrate_forces"):
		if _enabled:
			referer.connect("integrate_forces", self, "_integrate_forces")
		else:
			referer.disconnect("integrate_forces", self, "_integrate_forces"    )
	
func _supports(_node):
	return false

func _enter_state(_previous):
	pass
	
func _exit_state(_next):
	pass

func _integrate_forces(_state):
	pass
