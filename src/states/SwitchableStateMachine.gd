extends StateMachine
class_name SwitchableStateMachine


var selected_state = null

func _supports(_node):
	return true

func _enter_state(previous):
	if selected_state == null :
		for s in get_children():
			if s._supports(referer):
				change_state(s.name)
				break
	else:
		selected_state.set_enabled(true)
		selected_state._enter_state(previous)
	
func change_state(state_name):
	
	if get_tree().get_network_peer() and not is_network_master():
		return
		
	var next_state = get_node(state_name)
	
	if selected_state:
		selected_state.set_enabled(false)
		selected_state._exit_state(next_state)
	
	var previous_state = selected_state
	
	if next_state._supports(referer):
		selected_state = next_state
		selected_state.set_enabled(enabled)
		selected_state._enter_state(previous_state)
		
func _exit_state(next):
	if selected_state:
		selected_state.set_enabled(false)
		selected_state._exit_state(next)
