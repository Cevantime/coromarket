extends CanvasLayer

signal animation_finished()

onready var transition_color = $TransitionColor
onready var animation_player = $TransitionColor/AnimationPlayer

var current_music_path = null

func play_in(name = "shards", speed = 1.0):
	play(name, "in", speed)
	
func play_out(name = "shards", speed = 1.0):
	play(name, "out", speed)

func play(name = "shards", type = "out", speed = 1.0):
	transition_color.material.set_shader_param("mask", load("res://assets/shaders/masks/%s.png" % name))
	animation_player.play("transition_%s" % type, -1, speed)
	yield(animation_player, "animation_finished")
	emit_signal("animation_finished")
	
func transition(scene_name, name = "shards", speed = 1.0):
	play_out(name, speed)
	yield(self, "animation_finished")
	var _t = get_tree().change_scene(scene_name)
	play_in(name, speed)
	
func transition_to(scene, name = "shards", speed = 1.0):
	play_out(name, speed)
	yield(self, "animation_finished")
	var _t = get_tree().change_scene_to(scene)
	play_in(name, speed)
	
func play_music(stream):
	if stream is String:
		stream = load(stream)
	if stream.resource_path != current_music_path:
		current_music_path = stream.resource_path
		$AudioStreamPlayer.stream = stream
		$AudioStreamPlayer.play()
		
func stop_music():
	$AudioStreamPlayer.stop()
	current_music_path = null
