extends StaticBody

export(float) var translating_speed = 2.0

signal product_bought(product, player)

var character setget set_character
export(NodePath) var cashier_path

onready var cashier = get_node(cashier_path)

func _on_TranslateArea_body_entered(body):
	if "translating_velocity" in body:
		body.translating_velocity = -global_transform.basis.x * translating_speed

func _on_BuyArea_body_entered(body):
	if body.has_method("bought_by") and character and (not get_tree().network_peer or body.is_network_master()):
		if cashier:
			cashier.play("Cashier")
		body.bought_by(character)
		$AudioStreamPlayer3D.play()
		emit_signal("product_bought", body, character)
		
func set_character(c):
	if character:
		character.disconnect("finished_checkout", self, "_on_character_finished_checkout")
	character = c
	if not character.is_connected("finished_checkout", self, "_on_character_finished_checkout"):
		character.connect("finished_checkout", self, "_on_character_finished_checkout")
		
func _on_character_finished_checkout():
	set_character(null)
