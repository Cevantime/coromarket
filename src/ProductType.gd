tool
extends Resource

export(int) var nb_min_on_shopping_list = 0
export(int) var nb_max_on_shopping_list = 2
export(String) var name setget set_name
export(PackedScene) var scene setget set_scene

var Product = preload("res://src/Product.gd")
var index
var degree
var contamined
var dirty = false
var icon

signal modified(_self)

func set_scene(_scene):
	if _scene != null:
		var instance = _scene.instance()
		scene = _scene
		self.name = instance.product_name
		self.icon = instance.icon.resource_path
		

func set_name(_name):
	name = _name
	resource_name = _name
	emit_signal("modified", self)

func infos():
	return {
		"path" : scene.resource_path,
		"name" : name,
		"degree" : degree,
		"contamined" : contamined,
		"dirty": dirty,
		"icon": icon
	}
