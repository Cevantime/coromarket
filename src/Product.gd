extends RigidBody

signal collected()

enum STATES {
	FLOATING,
	IDLE,
	DROPPED,
	TO_TAKE,
	TAKEN,
	TO_LAUNCH,
	LAUNCHED,
	TRANSLATING,
	BEING_PUT
}

export(float) var TAKE_SPEED = 10.0
export(Texture) var icon
export(String) var product_name

var taker
var state = STATES.IDLE
var translating_velocity = Vector3.ZERO
var launch_impulse
var contamined = false
var mesh
var time_acc = 0
var degree = "standard"
var product_infos = {}
var dirty = false setget set_dirty

var audio_stream_player

func _ready():
	axis_lock_angular_x = true
	axis_lock_angular_z = true
	axis_lock_angular_y = true
	axis_lock_linear_x = true
	axis_lock_linear_z = true
	axis_lock_linear_y = true
	gravity_scale = 0
	collision_layer = 16
	collision_mask = 8
	contacts_reported = 1
	add_child(preload("res://src/NotifyNetwork.tscn").instance())
	add_to_group("product")
	for child in get_children():
		for c in child.get_children():
			if c is MeshInstance and c.get("material/0") != null:
				mesh = c
				break
	
	mesh.set("material/0", mesh.get("material/0").duplicate())
	
	if self.degree == "rupture":
		var rupture_effect = preload("res://ART/Effects/Scenes/SM_GodRays_01.tscn").instance()
		rupture_effect.set_name("RuptureEffect")
		mesh.get("material/0").set_shader_param("special_switch", 2)
		add_child(rupture_effect)
	elif self.degree == "promo":
		var promo_effect = preload("res://ART/Effects/Scenes/PSS_Sparkles.tscn").instance()
		promo_effect.set_name("PromoEffect")
		add_child(promo_effect)
	
	if has_node("AudioStreamPlayer"):
		audio_stream_player = $AudioStreamPlayer
	else:
		audio_stream_player = AudioStreamPlayer.new()
		add_child(audio_stream_player)
	
	audio_stream_player.bus = "FX"
	
	if contamined:
		audio_stream_player.stream = load("res://assets/sounds/pop-infected.wav")
		audio_stream_player.play()
		var covid_cells = preload("res://ART/Effects/Scenes/CovidCells.tscn").instance()
		add_child(covid_cells)
	elif degree == "rupture":
		audio_stream_player.stream = load("res://assets/sounds/graal.wav")
		audio_stream_player.play()
	else:
		audio_stream_player.stream = load("res://assets/sounds/pop-%s.wav" % [randi() % 3])
		audio_stream_player.play()
		
	if dirty:
		remove_shinyness()
	

func _process(delta):
	
	if state == STATES.FLOATING:
		time_acc += delta
		mesh.transform.origin.y = sin(time_acc * 5.0) * 0.075
	elif state == STATES.BEING_PUT:
		global_transform = taker.get_take_position().global_transform

func _integrate_forces(s):
	
	if get_tree().network_peer and not is_network_master():
		return
		
	var delta = 1 / s.get_step()
	var velocity = s.get_linear_velocity()
	
	if s.get_contact_count() > 0 and state == STATES.LAUNCHED:
		if get_tree().network_peer:
			rpc("touched")
		else : 
			touched()
	
	match state:
		STATES.TO_TAKE:
			if taker == null:
				state = STATES.IDLE
				return
			var diff_pos = taker.get_take_position().global_transform.origin - global_transform.origin

			if diff_pos.length() < TAKE_SPEED / delta:
				velocity = diff_pos * delta
				state = STATES.TAKEN

			else :
				velocity = TAKE_SPEED * diff_pos.normalized()
			
		STATES.TAKEN:
			if taker.product == null:
				dropped()
			else:
				s.transform = taker.get_take_position().global_transform
				
		STATES.TO_LAUNCH:
			velocity = launch_impulse
			s.apply_torque_impulse(Vector3(randf(), randf(), randf()) * 0.3)
			launch_impulse = null
			self.taker = null
			state = STATES.LAUNCHED
		STATES.TRANSLATING:
			velocity = translating_velocity
		STATES.DROPPED:
			s.add_central_force(Vector3(randf(), randf(), randf()) * 0.025)
			state = STATES.IDLE
			return
		
	if velocity:
		s.set_linear_velocity(velocity)

func set_dirty(d):
	dirty = d
	product_infos.dirty = true
	
func taken_by(_taker, network_id = null):
	if self.taker:
		return
	self.taker = _taker
	
	yield(get_tree().create_timer(0.2),"timeout")
	
	mesh.transform.origin.y = 0.0
	if network_id:
		rpc("add_taken_contraints", network_id)
	else:
		add_taken_contraints(network_id)

remotesync func add_taken_contraints(network_id = null):
	if network_id:
		set_network_master(network_id)
	can_sleep = false
#	$AnimationPlayer.stop()
	axis_lock_linear_x = false
	axis_lock_linear_z = false
	axis_lock_linear_y = false
	axis_lock_angular_x = true
	axis_lock_angular_z = true
	axis_lock_angular_y = true
	gravity_scale = 0
	collision_layer = 16
	collision_mask = 8
	state = STATES.TO_TAKE

remotesync func add_dropped_constraints():
	axis_lock_linear_x = false
	axis_lock_linear_z = false
	axis_lock_linear_y = false
	axis_lock_angular_x = false
	axis_lock_angular_z = false
	axis_lock_angular_y = false
	gravity_scale = 1
	collision_layer = 1 + 4 + 16
	collision_mask = 1 + 8

remotesync func add_launched_constraints():
	state = STATES.TO_LAUNCH
	axis_lock_angular_x = false
	axis_lock_angular_z = false
	axis_lock_angular_y = false
	axis_lock_linear_x = false
	axis_lock_linear_z = false
	axis_lock_linear_y = false
	gravity_scale = 1
	collision_layer = 1 + 4 + 16
	collision_mask += 1 + 2 + 8
	
remotesync func add_translating_constraints():
	
	axis_lock_angular_x = true
	axis_lock_angular_z = true
	axis_lock_angular_y = true
	axis_lock_linear_x = false
	axis_lock_linear_z = false
	axis_lock_linear_y = false
	gravity_scale = 5
	collision_layer = 1 + 4 + 16
	collision_mask += 1 + 2 + 8
	
func launched_by(_launcher, impulse):
	if not is_inside_tree():
		return
	
	if get_tree().network_peer:
		rpc("add_launched_constraints")
	else : 
		add_launched_constraints()
		
	
	launch_impulse = impulse

func start_translating():
	
	if get_tree().network_peer:
		rpc("add_translating_constraints")
	else:
		add_translating_constraints()

func is_collectable():
	return not (state in [STATES.FLOATING, STATES.TRANSLATING, STATES.BEING_PUT, STATES.TO_TAKE]) and (not get_tree().network_peer or is_network_master())

func being_put_by(character):
	taker = character
	if get_tree().network_peer:
		rpc("add_translating_constraints")
	else:
		add_translating_constraints()
	state = STATES.BEING_PUT
	
func put_by(_character):
	state = STATES.TRANSLATING
	
func add_smoke():
	var smoke = preload("res://ART/Effects/Scenes/SM_CloudsSmoke_01_B_1.tscn").instance()
	smoke.global_transform = global_transform
	get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1).add_child(smoke)
	smoke.start()
	
func bought_by(_character):
	if get_tree().network_peer:
		rpc("disappear")
	else: 
		disappear()
	
remotesync func disappear():
	add_smoke()
	call_deferred("queue_free")
	
func remove_shinyness():
	if has_node("PromoEffect"):
		get_node("PromoEffect").get_node("Particles").emitting = false
	if has_node("RuptureEffect"):
		get_node("RuptureEffect").queue_free()
	mesh.get("material/0").set_shader_param("takeable_switch", 0)
	
remotesync func dropped():
	state = STATES.DROPPED
	self.taker = null
	add_dropped_constraints()

remotesync func touched():
	remove_shinyness()
	state = STATES.IDLE
	self.dirty = true
	
remotesync func collected():
	
	disappear()
	emit_signal("collected")

