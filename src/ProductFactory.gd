class_name ProductFactory

static func build_product_from_infos(product_infos):
	var product = load(product_infos.path).instance()
	product.degree = product_infos.degree
	product.product_name = product_infos.name
	product.contamined = product_infos.contamined
	product.dirty = product_infos.dirty
	product.product_infos = product_infos
	product.set_name("product-"+product_infos.node_name)
	print('product built ', product_infos.node_name)
	return product
