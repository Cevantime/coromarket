extends StaticBody

export(bool) var enabled = true

var ShoppingKart = preload("res://src/ShoppingKart.gd")

var body_stack = []

func _on_Area_body_entered(body):
	if enabled and body is ShoppingKart:
		if body_stack.empty():
			$AnimationPlayer.play("open")
		body_stack.push_back(body)

func _on_Area_body_exited(body):
	if enabled and body is ShoppingKart:
		body_stack.remove(body_stack.bsearch(body))
		if body_stack.empty():
			$AnimationPlayer.play_backwards("open")
