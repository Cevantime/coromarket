extends Node

var timer_network
var nodes
var from_transforms = []
var target_transforms = []
var index_transform = 0

export(Array, NodePath) var nodes_to_notify

var waiting_nodes = []
var notifications_started = false

func _ready():
	call_deferred("start_notifications")

func start_notifications():
	if get_tree().get_network_peer():
		nodes = [get_parent()]
		for n in nodes_to_notify :
			var node = get_node(n)
			nodes.push_back(node)
		
		for n in (nodes + waiting_nodes):
			from_transforms.push_back(n.global_transform)
			target_transforms.push_back(n.global_transform)
			
		timer_network = Timer.new()
		add_child(timer_network)
		timer_network.wait_time = 0.2
		timer_network.start()
		timer_network.connect("timeout",self, "_notify_transform")
		notifications_started = true
		
func add_node_to_notify(node):
	if notifications_started:
		if node is NodePath:
			node = get_node(node)
		nodes.push_back(node)
		from_transforms.push_back(node.global_transform)
		target_transforms.push_back(node.global_transform)
	else :
		if node is NodePath:
			nodes_to_notify.push_back(node)
		else:
			waiting_nodes.push_back(node)
		
func _process(_delta):
	interpolate_transforms()

func _notify_transform():
	if get_tree().get_network_peer() and is_network_master():
		var transforms = []
		for n in nodes:
			transforms.push_back(n.global_transform if (is_instance_valid(n) and n.is_inside_tree()) else Transform.IDENTITY)
		rpc("update_transforms", transforms)
	
puppet func update_transforms(transforms):
	from_transforms = target_transforms
	target_transforms = transforms
	index_transform = 0
	

func interpolate_transforms():
	if get_tree().get_network_peer() and not is_network_master() and nodes:
		var i = 0
		index_transform += 1
		for node in nodes:
			if is_instance_valid(node):
				node.global_transform = from_transforms[i].interpolate_with(target_transforms[i], (1 / (timer_network.wait_time * Engine.get_frames_per_second())) * index_transform)
	#			node.global_transform = target_transforms[i]
			i += 1
