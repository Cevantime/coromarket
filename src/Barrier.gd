extends Spatial

func open():
	yield(get_tree().create_timer(randf() / 3), "timeout")
	$AnimationPlayer.play("open")
