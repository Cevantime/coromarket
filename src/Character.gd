tool
extends KinematicBody
class_name Character

const POWER_MAX = 30

signal assigned_shopping_kart(_self)
signal finished_checkout()

enum STATES {
	RUNNING,
	PUSHING,
	MOVING_TO_PUSH_POSITION,
	ROTATING_TO_PUSH_POSITION
}

enum COLORS {
	BLUE,
	RED,
	GREEN,
	YELLOW
}

export(PackedScene) var body_scene = preload("res://src/placeholders/FakeBody.tscn") setget set_body_scene
export(PackedScene) var eyes_scene setget set_eyes_scene
export(PackedScene) var mouth_scene setget set_mouth_scene
export(PackedScene) var hand_scene setget set_hand_scene
export(PackedScene) var hat_scene setget set_hat_scene
export(float) var SPEED = 10.0
export(float) var ROTATE_SPEED = 200.0
export(int) var POWER_SPEED = 40
export(bool) var CONTROLLED = false
export(float) var HANDS_ANGLE_FACTOR = 0.03
export(int, 1, 10) var MASH_NUMBER_TO_PUT_OBJECT = 3
export(int) var device = 0
export(COLORS) var color_edited = 1 setget set_color_edited
var color_values = [
	preload("res://ART/Material/Character/M_CharacterPlayerColor_A_01.tres"),
	preload("res://ART/Material/Character/M_CharacterPlayerColor_A_02.tres"),
	preload("res://ART/Material/Character/M_CharacterPlayerColor_A_04.tres"),
	preload("res://ART/Material/Character/M_CharacterPlayerColor_A_03.tres"),
]

export(float) var MIN_POWER = 7.0

var player_name
var target_position
var validated_products = []
var angle_hands = 0
var selected_stand setget ,get_selected_stand
var selected_stands = []
var product = null
var pickable_product = null
var Product = preload("res://src/Product.gd")
var original_y
var shopping_kart
var shopping_kart_in_range = null
var shopping_karts_in_range_for_deposit = []
var angle = null
var velocity = Vector3.ZERO
var color
var dir
var material
var power = 0
var power_max = POWER_MAX
var strike_left = true
var shopping_list = []
var animation_playing = false
var has_checked_out = false
var circle
var color_meshes = {}
var players_to_hit_stack = []
var state setget , get_state
var end_transform
var u_id

var previous_transform

func _ready():
	original_y = global_transform.origin.y
#	$Body.set("material/0", $Body.get("material/0").duplicate())
	set_color_edited(color_edited)
	set_body_scene(body_scene)
	circle = $CharacterCircle_01.get_child(0)
	circle.set("material/0", circle.get("material/0").duplicate())
	if get_tree().network_peer:
		$NotifyNetwork.add_node_to_notify($BodyPosition/Body/Hands)
	
	
func change_state(state_name):
	if not Engine.editor_hint:
		$States.change_state(state_name)
	
func go_to_end_position(e_transform):
	if get_tree().network_peer:
		rpc("play", "applause")
	else :
		play("applause")
	end_transform = e_transform
	change_state("End")

func get_device_str():
	return str(device) if device > 0 else ''

func get_state():
	return $States.selected_state
	
func move_to_position(position_node):
	target_position = position_node
	change_state("MovingToPosition")

func set_color_edited(_color):
	color_edited = _color
	material = color_values[_color]
	for meshes in color_meshes.values():
		for m in meshes:
			m.set("material/0", material)
#	if has_node('Body') and is_instance_valid($Body) :
#		$Body.get("material/0").set("albedo_color", albedo)
	color = int(pow(2, _color))
	
	$CharacterCircle_01.get_child(0).get("material/0").set_shader_param("Color", get_albedo())
	
func get_albedo():
	return material.get_shader_param("ColorLight")

func get_selected_stand():
	if $HitRaycast.is_colliding():
		for stand in selected_stands:
			if stand == $HitRaycast.get_collider() and stand.product:
				return stand
	
	for stand in selected_stands:
		if stand.product != null:
			return stand
	
	return null
	
func _process(_delta):
	if state == $States/Running:
		collision_layer = 1 + 2
		if shopping_kart and shopping_kart.CONTROLLED:
			shopping_kart.CONTROLLED = false
	
	if Engine.editor_hint and not circle :
		circle = $CharacterCircle_01/CharacterCircle_01
	var hands = get_body().get_node("Hands")
	angle_hands = lerp_angle(angle_hands, power * HANDS_ANGLE_FACTOR, 0.5)
	hands.transform.basis = Basis()
	hands.rotate_z(angle_hands)
	circle.get("material/0").set_shader_param("Charging", power/POWER_MAX)
	if Engine.editor_hint and get_body().get_child_count() == 0:
		set_body_scene(body_scene)
	
	circle.get_parent().global_transform.basis = Basis()
	
	
func run() :
	change_state("Running")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	if angle != null:
		var target_transform = Transform(global_transform)
		target_transform.basis = Basis()
		target_transform = target_transform.rotated(Vector3.DOWN, angle)
		
		var quat1 = global_transform.basis.get_rotation_quat()
		var quat2 = target_transform.basis.get_rotation_quat()
		
		var result = quat1.slerp(quat2, ROTATE_SPEED * delta)
		
		global_transform.basis = Basis(result)
		global_transform = global_transform.orthonormalized()
	
	velocity = move_and_slide(velocity)
	
		


func assign_shopping_kart(s_kart = null):
	shopping_kart = self.shopping_kart_in_range if not s_kart else s_kart
	shopping_kart.connect("arrived_to_checkout", self, "_on_arrived_to_checkout")

	if get_tree().get_network_peer():
		shopping_kart.rpc("assign_character", null, get_tree().get_network_unique_id())
	else:
		shopping_kart.assign_character(self)
	

func _on_arrived_to_checkout():
	if shopping_kart.products.empty():
		finish()
		return
	change_state("PutObjects")

remote func pop_object(id = null, product_infos= {}):
	play("put_object")
	var p = ProductFactory.build_product_from_infos(product_infos)
	var scene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)
	scene.add_child(p)
	if id :
		p.set_network_master(int(id))
	p.global_transform = get_take_position().global_transform
	return p

func put_next_object():
	var product_infos = shopping_kart.products.pop_back()
	var p
	if get_tree().network_peer:
		rpc("pop_object", get_tree().get_network_unique_id(), product_infos)
		p = pop_object(get_tree().get_network_unique_id(), product_infos)
	else: 
		p = pop_object(null, product_infos)
		
	p.being_put_by(self)
	
	yield(get_tree().create_timer(0.15), "timeout")
	
	if is_instance_valid(p) and p != null :
		p.put_by(self)

	if shopping_kart.products.empty():
		finish()
		return
	
remote func request_take_shopping_kart(player_id, shopping_kart_id):
	var player = instance_from_id(player_id)
	var sk = instance_from_id(shopping_kart_id)
	if sk.player == null or sk.player == player:
		player.rpc("take_shopping_kart", sk)
	
remote func take_shopping_kart(s_kart = null):
	if s_kart:
		s_kart = instance_from_id(s_kart)
	if not shopping_kart:
		assign_shopping_kart(s_kart)
	$AudioStreamPlayer.stream = preload("res://assets/sounds/take_shopping_kart.wav")
	$AudioStreamPlayer.play()
	shopping_kart.taken_by_player()
	change_state("MovingToPushPosition")
	collision_layer = 2
	
func take_off_shopping_kart():
	if not shopping_kart:
		return
	shopping_kart.leaved_by_player()
	collision_layer = 1 + 2
	
func leave_shopping_kart():
	take_off_shopping_kart()
	run()
	

remote func take_stand_product(stand):
	if typeof(stand) == TYPE_INT:
		stand = instance_from_id(stand)
		
	if stand.product and ! product:
		var p = stand.product
		if get_tree().get_network_peer():
			stand.rpc("unset_product")
		else:
			stand.unset_product()
		if get_tree().get_network_peer(): 
			rpc("play", "take")
		else:  
			play("take")
		take_product(p)

remotesync func play(animation):
	var anim_player = get_body().get_node("AnimationPlayer")
	if anim_player.is_playing() and ["strike_left", "strike_right", "running"].has(anim_player.current_animation):
		yield(anim_player, "animation_finished")
	anim_player.play(animation)
	
func get_body():
	return $BodyPosition/Body
	
func strike():
	$AudioStreamPlayer.stream = preload("res://assets/sounds/strike.wav")
	$AudioStreamPlayer.play()
	strike_left = !strike_left
	if get_tree().get_network_peer():
		rpc("play", "strike_%s" % ('left' if strike_left else 'right'))
	else:
		play("strike_%s" % ('left' if strike_left else 'right'))
	for target in players_to_hit_stack:
		if get_tree().get_network_peer():
			target.rpc("hit")
		else :
			target.hit()
	
remote func request_take_stand_product(player_id, stand_id):
	var player = instance_from_id(player_id)
	var stand = instance_from_id(stand_id)
	if stand.product:
		stand.product = null
		player.rpc(player_id, "take_stand_product", stand)
		
remote func request_pickup_product(player_id, product_id):
	var player = instance_from_id(player_id)
	var p = instance_from_id(product_id)
	if not p.player:
		p.player = player
		player.rpc(player_id, "pickup_product", p)

remote func pickup_product(p = null):
	if not p:
		p = pickable_product
	else :
		p = instance_from_id(p)
	take_product(p)
	pickable_product = null
	if get_tree().get_network_peer(): 
		rpc("play", "pickup")
	else:  
		play("pickup")
		
func drop_product():
	if product and is_instance_valid(product):
		if get_tree().get_network_peer():
			product.rpc("dropped")
		else :
			product.dropped()
		
		product = null

func take_product(_product):
	if not is_instance_valid(_product):
		return
	change_state("TakeProduct")
	product = _product
	if not product.is_connected("collected", self, "_on_product_collected"):
		product.connect("collected", self, "_on_product_collected")
	if get_tree().get_network_peer():
		product.taken_by(self, get_tree().get_network_unique_id())
	else:  
		product.taken_by(self, null)
		
func launch():
	if ! product or not is_instance_valid(product):
		return
	
	$AudioStreamPlayer.stream = preload("res://assets/sounds/throw.wav")
	$AudioStreamPlayer.play()
	
	if get_tree().get_network_peer():
		rpc("play", "launch")
	else:  
		play("launch")
		
	product.launched_by(self, (dir.rotated(global_transform.basis.z, 0.15).normalized()) * power)
	product = null

func get_take_position():
	return get_body().get_node("Hands/TakePosition")
	
func deposit_product():
	if not product or shopping_karts_in_range_for_deposit.empty():
		return
	change_state("Deposit")

func finish():
	if get_body().get_node("AnimationPlayer").is_playing():
		yield(get_body().get_node("AnimationPlayer"), "animation_finished")
	
	play("take_shopping_kart")
	shopping_kart.back_to_normal()
	shopping_kart.CONTROLLED = true
	take_shopping_kart()
	has_checked_out = true
	change_state("Finished")
	
remotesync func hit():
	if get_state() and get_state().name == "Locked":
		return
	var previous_state = null if get_state() == null else get_state().name
	drop_product()
	take_off_shopping_kart()
	change_state("Locked")
	$AudioStreamPlayer.stream = preload("res://assets/sounds/stun.wav")
	$AudioStreamPlayer.play()
	play("hit")
	yield(get_tree().create_timer(1.5), "timeout")
	get_body().get_node("AnimationPlayer").stop()
	get_body().get_node("BirdsStuned_01_A_1").hide()
	change_state("Running" if (previous_state != "PutObjects" and previous_state != "Finished") else previous_state)

func push_stand(stand):
	if selected_stands.find(stand) == - 1:
		selected_stands.push_back(stand)

func remove_stand(stand):
	var stand_index = selected_stands.find(stand)
	if stand_index >= 0:
		selected_stands.remove(stand_index)
		
func set_eyes_scene(_eyes_scene):
	eyes_scene = _eyes_scene
	add_to_position(_eyes_scene, get_body().get_node("BodyPosition/Face/EyesPosition"))

func set_mouth_scene(_mouth_scene):
	mouth_scene = _mouth_scene
	add_to_position(_mouth_scene, get_body().get_node("BodyPosition/Face/MouthPosition"))
	
func set_hand_scene(_hand_scene):
	hand_scene = _hand_scene
	add_to_position(_hand_scene, get_body().get_node("Hands/LeftHand/LeftHandPostion"))
	add_to_position(_hand_scene, get_body().get_node("Hands/RightHand/RightHandPosition"))
	
	
func set_hat_scene(_hat_scene):
	hat_scene = _hat_scene
	add_to_position(_hat_scene, get_body().get_node("BodyPosition/HatPosition"))
	
func set_body_scene(_body_scene):
	body_scene = _body_scene
	if $BodyPosition.get_child_count() > 0:
		$BodyPosition.get_child(0).set_name("OldBody")
	add_to_position(_body_scene, $BodyPosition, "Body")
	get_body().set_network_master(get_network_master())
	if not is_inside_tree():
		return
	set_mouth_scene(mouth_scene)
	set_hand_scene(hand_scene)
	set_hat_scene(hat_scene)
	set_eyes_scene(eyes_scene)
	$CollisionShape.shape = get_body().get_node("CollisionShape").shape

func add_to_position(scene, position_node, name = null):
	if not scene:
		color_meshes[position_node.get_name()] = []
		for c in position_node.get_children():
			c.call_deferred("queue_free")
		return
	var s = scene.instance()
	var meshes = find_color_material_nodes(s)
	color_meshes[position_node.get_name()] = meshes
	for m in meshes:
		m.set("material/0", material)
	if name != null:
		s.set_name(name)
	for c in position_node.get_children():
		c.call_deferred("queue_free")
	position_node.add_child(s)

func find_color_material_nodes(node):
	var from = []
	for c in node.get_children():
		from += find_color_material_nodes(c)
		if c.is_in_group("color_node"):
			from.push_back(c)
	return from
	
remotesync func add_product_to_shopping_list(p):
	shopping_list.push_back(p)
	
func _on_HitBox_body_entered(body):
	if body != product and body is Product:
		var body_state = body.state
		if body_state == Product.STATES.LAUNCHED:
			if get_tree().get_network_peer():
				rpc("hit")
				body.rpc("touched")
			else:
				hit()
				body.touched()


func _on_PickUpArea_body_entered(body):
	if body is Product and not pickable_product:
		pickable_product = body

func _on_PickUpArea_body_exited(body):
	if body == pickable_product:
		pickable_product = null  


func _on_AnimationPlayer_animation_finished(_anim_name):
	animation_playing = false
	
func _on_product_collected():
	if product and is_instance_valid(product):
		product.disconnect("collected", self, "_on_product_collected")
		product = null
		if get_state() == $States/Deposit:
			change_state("Running")
		

func _on_StrikeBox_area_entered(area):
	var body = area.get_parent()
	
	if area.name == "HitBox" and body != self and body.has_method("hit") and is_instance_valid(body):
		players_to_hit_stack.push_back(body)


func _on_StrikeBox_area_exited(area):
	var body = area.get_parent()
	if area.name == "HitBox" and body != self and body.has_method("hit") and is_instance_valid(body):
		players_to_hit_stack.remove(players_to_hit_stack.find(body))


func _on_TimerSmoke_timeout():
	if previous_transform == null:
		previous_transform = global_transform
	for p in get_body().get_node("WalkSmoke").get_children():
		p.emitting = (global_transform.origin - previous_transform.origin).length() > 0.1

	previous_transform = global_transform
