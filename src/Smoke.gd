extends Spatial

var tween: Tween = Tween.new()
var interpolated_explode_factor = 0 setget set_interpolated_explode_factor
export(float) var explode_duration = 0.25


func start():
	add_child(tween)
	var _m = tween.interpolate_property(self, "interpolated_explode_factor", 0, 1, explode_duration, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	var _s = tween.start()
	yield(tween,"tween_completed")
	call_deferred("queue_free")

func set_interpolated_explode_factor(f):
	get_child(0).get("material/0").set_shader_param("ExplodeFrag", f)
	get_child(0).get("material/0").set_shader_param("ExplodeFrag2", f)
	interpolated_explode_factor = f
