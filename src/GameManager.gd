tool
extends Node

signal counted_down(time)
signal timed_down(time)
signal game_timeout()
signal game_started()
signal game_begun()
signal checkout_began()
signal game_finished()
signal score_updated(player, score)

class ProductQueue :
	var name
	var queue = []
	
export(Array, Resource) var product_classes
export(int) var nb_rupture = 1
export(int) var nb_promo = 2
export(int) var nb_pop_rupture = 1
export(int) var nb_pop_promo = 1
export(int) var nb_max_products_on_list_by_player = 8
export(int) var nb_min_products_on_list_by_player = 5
export(int, 0, 10) var initial_count_down = 3
export(float, 0.5, 4) var count_down_time = 1.2
export(int) var score_new = 100
export(float) var score_promo = 2
export(float) var score_rupture = 5
export(float) var score_dirty = 0.5
export(float) var score_contamined = -1
export(float) var score_in_list = 2
export(float) var malus_timeout_by_sec = 30
export(int) var initial_number_of_products = 10
export(int) var total_number_of_products = 40
export(float) var contaminated_rate = 0.05
export(int) var game_duration_in_secs = 120
export(int) var checkout_duration_in_secs = 30
export(int) var end_duration_in_secs = 30
export(bool) var randomized = true
export(int) var pop_interval_secs = 5
export(bool) var autostart = false

var promo_indexes = []
var rupture_indexes = []
var standard_indexes = []

var promo_queue
var rupture_queue
var standard_queue
var game_finished = false
var count_down
var queues = []
var queue_index = 0
var player_scores = {}

var ShoppingKart = preload("res://src/ShoppingKart.gd")

var finished_players = {}
var players_stored_list = {}

onready var remaining_time = game_duration_in_secs
onready var product_classes_size = product_classes.size()
onready var pop_timer = Timer.new()
onready var game_timer = Timer.new()
onready var count_down_timer = Timer.new()
onready var end_game_timer = Timer.new()

var index_product = 0

func _ready():
	randomize()
	count_down = initial_count_down
	remaining_time = game_duration_in_secs
	
	if autostart and not Engine.editor_hint:
		if not get_tree().get_network_peer() or (get_tree().get_network_peer() and get_tree().is_network_server()):
			start_game()
			

func set_players_scores():
	for player in get_tree().get_nodes_in_group("player"):
		player_scores[player.get_name()] = 0
	
	for checkout in get_tree().get_nodes_in_group("checkout"):
		if not(checkout.is_connected("product_bought", self, "_on_product_bought")):
			checkout.connect("product_bought", self, "_on_product_bought")
		
func _process(_delta):
	if Engine.editor_hint:
		if product_classes_size == null:
			product_classes_size = 0
		elif product_classes.size() > product_classes_size:
			for _i in range(product_classes_size, product_classes.size()):
				var product_type = (preload("res://src/ProductType.tres")).duplicate()
				product_classes[_i] = product_type
				product_type.index = _i
				product_type.resource_name = "Product Type"
#				product_type.take_over_path("res://src/ProductType.tres")
		
		if product_classes_size != product_classes.size():
			product_classes_size = product_classes.size()
			
			
#		for _i in range(product_classes_size):
#			var pc = product_classes[_i]
#			if pc != null and pc.resource_path != "":
#				print(pc.resource_path)
#				print("new resource")
#				print(pc)
#				var product_type = (preload("res://src/ProductType.tres")).duplicate()
#				product_classes[_i] = product_type
#				product_type.index = _i
#				product_type.scene = pc
#				product_type.connect("modified", self, "_on_ProductType_modified")


func on_count_down():
	if get_tree().network_peer:
		rpc("emit_count_down", count_down)
	else :
		emit_count_down(count_down)
	count_down -= 1
	if count_down < 0:
		count_down_timer.stop()
		game_timer.start()
		if get_tree().network_peer:
			rpc("emit_game_begun")
		else :
			emit_game_begun()
		
remotesync func emit_count_down(c_down):
	emit_signal("counted_down", c_down)
	
remotesync func emit_game_begun():
	emit_signal("game_begun")
	
func time_down():
	remaining_time -= 1
	if get_tree().network_peer:
		rpc("emit_timed_down", remaining_time)
	else :
		emit_timed_down(remaining_time)
	if remaining_time == checkout_duration_in_secs:
		if get_tree().network_peer:
			rpc("emit_checkout_began")
		else: 
			emit_checkout_began()
	if remaining_time == 0:
		game_timer.stop()
		end_game_timer.start()
		if get_tree().network_peer:
			rpc("emit_game_timeout")
		else:
			emit_game_timeout()

remotesync func emit_timed_down(r_time):
	emit_signal("timed_down", r_time)

remotesync func emit_checkout_began():
	emit_signal("checkout_began")
	
remotesync func emit_game_timeout():
	emit_signal("game_timeout")

func time_down_end():
	remaining_time -= 1
	if get_tree().network_peer:
		rpc("emit_timed_down", remaining_time)
	else:
		emit_timed_down(remaining_time)
	if abs(remaining_time) > end_duration_in_secs:
		if not get_tree().network_peer or get_tree().is_network_server():
			finish_game()

func compute_product_score(player, product):
	if not players_stored_list.has(player.get_name()): 
		players_stored_list[player.get_name()] = player.shopping_list
		
	var product_list = players_stored_list[player.get_name()]
	
	var factor_in_list = 1
	
	var index_found = -1
	var i = 0
	for p in product_list:
		if p.name == product.product_name:
			factor_in_list = score_in_list
			index_found = i
			break
		i +=1 
	
	if index_found >= 0:
		players_stored_list[player.get_name()].remove(index_found)
	
	var factor_special = 1
	if product.degree == "promo":
		factor_special = score_promo
	elif product.degree == "rupture":
		factor_special = score_rupture
	
	var factor_dirty = score_dirty if product.dirty else 1
	var factor_contamined = score_contamined if product.contamined else 1
	
	return score_new * factor_special * factor_dirty * factor_contamined * factor_in_list
	

master func create_shopping_lists():
	
	for player in get_tree().get_nodes_in_group("player"):
		var nb_product_for_player = nb_min_products_on_list_by_player + randi() % (nb_max_products_on_list_by_player - nb_min_products_on_list_by_player)
		var products_available = [] + product_classes
		var nb_max_on_shopping_list_cache = {}
		for _i in range(nb_product_for_player):
			var product_index = randi() % products_available.size()
			var product_class = products_available[product_index]
			var nb_max_on_shopping_list
			if nb_max_on_shopping_list_cache.has(product_class.name):
				nb_max_on_shopping_list = nb_max_on_shopping_list_cache[product_class.name]
			else: 
				nb_max_on_shopping_list = product_class.nb_max_on_shopping_list
				
			if nb_max_on_shopping_list == 0:
				products_available.remove(product_index)
				continue
			nb_max_on_shopping_list -= 1
			if nb_max_on_shopping_list == 0:
				products_available.remove(product_index)
				
			nb_max_on_shopping_list_cache[product_class.name] = nb_max_on_shopping_list
			
			if get_tree().network_peer:
				player.rpc("add_product_to_shopping_list", product_class.infos())
			else:
				player.add_product_to_shopping_list(product_class.infos())
			
	
func start_game():
	if not get_tree().network_peer or get_tree().is_network_server():
		var indexes = range(product_classes.size())
		
		if randomized : 
			indexes.shuffle()
		if indexes.size() < nb_promo + nb_rupture:
			printerr("Number of products should be more than nb_promo + nb_rupture")
		for _i in range(nb_rupture):
			rupture_indexes.push_back(indexes.pop_front())
		for _i in range(nb_promo):
			promo_indexes.push_back(indexes.pop_front())
		for i in range(indexes.size()):
			standard_indexes.push_back(indexes[i])
			
		create_queues()
		
		create_shopping_lists()
		
		for _i in range(initial_number_of_products):
			pop_from_queue(2)
	
		pop_timer.wait_time = pop_interval_secs
		add_child(pop_timer)
		pop_timer.start()
		pop_timer.connect("timeout", self, "pop_product")
	
		count_down_timer.wait_time = count_down_time
		add_child(count_down_timer)
		count_down_timer.connect("timeout", self, "on_count_down")
		count_down_timer.start()
		
		game_timer.wait_time = 1
		add_child(game_timer)
		game_timer.connect("timeout", self, "time_down")
		
		end_game_timer.wait_time = 1
		add_child(end_game_timer)
		end_game_timer.connect("timeout", self, "time_down_end")
		
	set_players_scores()
	
	for p in get_tree().get_nodes_in_group("player"):
		p.add_to_group("camera_tracked")
	
	for exit in get_tree().get_nodes_in_group("exit"):
		exit.connect("body_entered", self, "_on_exit_entered")
		
	if get_tree().network_peer and get_tree().is_network_server():
		rpc("notify_game_started")
	elif not get_tree().network_peer :
		notify_game_started()
	
remotesync func notify_game_started():
	emit_signal("game_started")

func _on_exit_entered(body):
	if body is ShoppingKart:
		var player = body.character
		body.character.remove_from_group("camera_tracked")
		body.hide()
		body.character.hide()
		finished_players[body.character.name] = body.character
		if remaining_time < 0:
			if get_tree().network_peer:
				rpc("change_player_score", player.name, malus_timeout_by_sec * remaining_time)
			else :
				change_player_score(player.name, malus_timeout_by_sec * remaining_time)

		
	if not get_tree().network_peer or get_tree().is_network_server():
		if finished_players.size() == player_scores.size():
			finish_game()
		
		
func finish_game():
	if game_finished:
		return
		
	end_game_timer.stop()
	
	for p in get_tree().get_nodes_in_group("player"):
		var score = player_scores[p.name]
		
		if not finished_players.has(p.name):
			score -= malus_timeout_by_sec * end_duration_in_secs
				
		if score < 0:
			score = 0
			
		if get_tree().network_peer:
			rpc("change_player_score", p.name, score - player_scores[p.name])
		else :
			change_player_score(p.name, score - player_scores[p.name])
	
	game_finished = true
	if get_tree().network_peer:
		rpc("emit_game_finished")
	else:
		emit_game_finished()
	
remotesync func emit_game_finished():
	emit_signal("game_finished")

master func create_queues():
	promo_queue = ProductQueue.new()
	promo_queue.name = "promo"
	rupture_queue = ProductQueue.new()
	rupture_queue.name = "rupture"
	standard_queue = ProductQueue.new()
	standard_queue.name = "standard"
	
	var nb_products = 0
	for i in range(rupture_indexes.size()):
		for _j in range(nb_pop_rupture):
			rupture_queue.queue.push_back(rupture_indexes[i])
			nb_products += 1
			if nb_products == total_number_of_products:
				return
	for i in range(promo_indexes.size()):
		for _j in range(nb_pop_promo):
			promo_queue.queue.push_back(promo_indexes[i])
			nb_products += 1
			if nb_products == total_number_of_products:
				return
	while not standard_indexes.empty() and nb_products < total_number_of_products:
		if randomized:
			standard_queue.queue.push_back(standard_indexes[randi() % standard_indexes.size()])
			nb_products += 1
		else:
			for i in range(standard_indexes):
				standard_queue.queue.push_back(standard_indexes[i])
				nb_products += 1
	
	for q in [rupture_queue, promo_queue, standard_queue]:
		if not q.queue.empty():
			queues.push_back(q)

master func pop_from_queue(index_queue):
	var queue_to_pop_from = queues[index_queue]
	
	var product_type = product_classes[queue_to_pop_from.queue.pop_front()]
	
	if queue_to_pop_from.queue.empty():
		queues.remove(index_queue)
		
	if not product_type:
		return
	
	product_type.degree = queue_to_pop_from.name
	product_type.contamined = randf() < contaminated_rate
	
	var packed_scene = product_type.scene
	var product = packed_scene.instance()
	
	var product_groups = product.get_groups()
	
	var poppers = []
	
	for product_group in product_groups:
		for popper in get_tree().get_nodes_in_group("%s_popper" % product_group):
			if poppers.find(popper) >= 0 or popper.product != null:
				continue
			poppers.push_back(popper)
	
	if poppers.empty():
		return
	
	poppers.shuffle()
	
	var popper = poppers.pop_front()
	
	while not poppers.empty() and popper.product != null:
		popper = poppers.pop_front()
		
	if popper.product != null:
		return
		
	var product_node_name = str(index_product)
	var infos = product_type.infos()
	infos.node_name = product_node_name
	
	index_product += 1
	
	if get_tree().get_network_peer():
		print("must pop %s from %s" % [ infos.node_name, popper.name ])
		
		popper.rpc("pop_product", infos)
	else :
		popper.pop_product(infos)

master func pop_product():

	if queues.empty():
		return
	var index_queue
	
	if randomized:
		index_queue = randi() % queues.size()
	else:
		index_queue = queue_index
		queue_index = (queue_index + 1) % queues.size()
	
	pop_from_queue(index_queue)

remotesync func change_player_score(id, score):
	player_scores[id] += score
	
	emit_signal("score_updated", id, player_scores[id])
	
func _on_product_bought(product, player):
	if game_finished:
		return
	if get_tree().network_peer:
		rpc("change_player_score", player.get_name(), compute_product_score(player, product))
	else :
		change_player_score(player.get_name(), compute_product_score(player, product))
	

