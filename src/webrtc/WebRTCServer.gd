extends Node


class GameSession:
	var name
	var server_id
	var password
	var members = {}
	var created_time
	
	func _init():
		created_time = OS.get_system_time_secs()
		
	func must_be_closed():
		return OS.get_system_time_secs() - created_time > 300
	
	func infos():
		return {
			"name": name, 
			"server_id": server_id,
			"password": password,
			"members": members.size()
		}


class SessionMember:
	var id
	var session_name


const PORT = 9080

var server: WebSocketServer = WebSocketServer.new()

var sessions = {}
var members = {}

func _ready():
	var _c = server.connect("client_connected", self, "_connected")
	_c = server.connect("client_disconnected", self, "_disconnected")
	_c = server.connect("client_close_request", self, "_close_request")
	_c = server.connect("data_received", self, "_on_data")
	
	var err = server.listen(PORT)
	if err != OK:
		print("Unable to start server")
		set_process(false)
		
	var timer_check_session_expired = Timer.new()
	timer_check_session_expired.wait_time = 10.0
	add_child(timer_check_session_expired)
	timer_check_session_expired.connect("timeout", self, "_on_timer_check_session_expired_timeout")
	timer_check_session_expired.start()
	
	
func _connected(id, proto):
	print("Client %d connected with protocol: %s" % [id, proto])
	var member = SessionMember.new()
	member.id = id
	members[id] = member
	
	
func _disconnected(id, was_clean = false):
	var member = members[id]
	members.erase(id)
	if sessions.has(member.session_name):
		var session = sessions[member.session_name]
		if session.members.has(member.id):
			if member.id == session.server_id:
				sessions.erase(id)
				for m in session.members.values():
					send_msg(m.id, "session_closed", {})
			else:
				session.members.erase(member.id)
				
	print("Client %d disconnected, clean: %s" % [id, str(was_clean)])
	
	
func _close_request(id, code, reason):
	print("Client %d disconnecting with code: %d, reason: %s" % [id, code, reason])
	

func _on_data(id):
	print('data received from id %s' % id)
	var msg = get_msg(id)
	print("msg received : " + str(msg))
#
	match msg.type:
		"get_infos":
			var sessions_infos = []
			for s in sessions.values():
				if s.members.size() < 4:
					sessions_infos.push_back(s.infos())
			send_msg(id, "infos", {
				"sessions" : sessions_infos,
				"id": id
			})
		"register":
			var session_name = msg.data.name
			if sessions.has(session_name) :
				var session = sessions[session_name]
				send_msg(id, "already_registered", session.infos())
			else :
				var session = GameSession.new()
				session.name = session_name
				if msg.data.has('password'):
					session.password = msg.data.password
				else:
					session.password = ""
				sessions[session_name] = session
				var member  = members[id]
				member.session_name = session.name
				session.members[id] = member
				session.server_id = id
				send_msg(id, "registered", session.infos())
		"join":
			var session_name = msg.data.name
			if not (sessions.has(session_name)) :
				send_msg(id, "invalid_name", {"name":session_name})
			elif sessions[session_name].password != "" and sessions[session_name].password != null and (not msg.data.has('password') or msg.data.password != sessions[session_name].password):
				send_msg(id, "invalid_password", {"name":session_name, "password": msg.data.password})
			else:
				var session = sessions[session_name]
				var session_infos = session.infos()
				session_infos.is_server = session.server_id == id
				send_msg(id, "joined", session_infos)
				
				for k in session.members:
					var m = session.members[k]
					send_msg(m.id, "new_player", {
						"id": id
					})
					send_msg(id, "new_player", {
						"id": m.id
					})
				
				var member  = members[id]
				member.session_name = session.name
				session.members[id] = member
				
		"close":
			if not (msg.data.name in sessions):
				send_msg(id, "invalid_session_closed", {"name": msg.data.name})
			else :
				var session = sessions[msg.data.name]
				
				for m in session.members.values():
					send_msg(m.id, "session_closed", {})
					
				sessions.erase(msg.data.name)
				
				
		"offer":
			msg.data.from_id = id
			send_msg(msg.data.target_id, "offer", msg.data)
			
		"answer":
			msg.data.from_id = id
			send_msg(msg.data.target_id, "answer", msg.data)
			
		"candidate":
			msg.data.from_id = id
			send_msg(msg.data.target_id, "candidate", msg.data)
			
			
				

func _on_timer_check_session_expired_timeout():
	for k in sessions:
		var session = sessions[k]
		if session.must_be_closed():
			for m in session.members.values():
				send_msg(m.id, "session_closed", {name: session.name})
			sessions.erase(k)

func get_msg(id):
	return JSON.parse(server.get_peer(id).get_packet().get_string_from_utf8()).result

#func broadcast_msg(type, msg):
#	for client in clients_connected.values():
#		send_msg(client.id, type, msg)

func send_msg(id, type, msg):
	if server.has_peer(id): 
		server.get_peer(id).put_packet(JSON.print({"type": type, "data" : msg}).to_utf8())
	
	
func _physics_process(delta):
	server.poll()
