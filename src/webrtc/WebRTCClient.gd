extends Node


export var websocket_url = "ws://localhost:9080"

var is_server = false
var session_name
var id
var player_options
var client

signal infos_obtained(infos)
signal session_joined()
signal invalid_session_closed(name)
signal invalid_session_joined(name)
signal invalid_session_password(name, password)
signal already_registered_session(infos)
signal session_registered(infos)
signal new_player(id, peer)
signal offer_created(type, data, id)
signal offer_received(id, offer)
signal answer_received(id, answer)
signal candidate_received(id, mid, index, sdp)
signal session_closed()

signal started()

func _ready():
	start()
		
func _closed(was_clean = false):
	print("Closed, clean: ", was_clean)
	set_process(false)

func _connected(proto = ""):
	print("Connected with protocol: ", proto)
	client.get_peer(1).set_write_mode(WebSocketPeer.WRITE_MODE_TEXT)
	send_msg("get_infos")
	
func _disconnected():
	print("disconnected from server")
	emit_signal("disconnected_from_server")
	
func register_session(p_options, name, password):
	create_or_join(p_options, name, password, "register")
	
func join_session(p_options, name, password):
	create_or_join(p_options, name, password, "join")
	
func create_or_join(p_options, name, password, verb = "register"):
	player_options = p_options
	send_msg(verb, { 
		"name": name, 
		"password": password
	})
	
func get_infos():
	send_msg("get_infos")
	
func close_session():
	send_msg("close", { name : session_name })
	
func start():
	set_process(true)
	client = WebSocketClient.new()
	var _c = client.connect("connection_closed", self, "_closed")
	_c = client.connect("connection_error", self, "_closed")
	_c = client.connect("connection_established", self, "_connected")
	_c = client.connect("server_disconnected", self, "_disconnected")
	_c = client.connect("data_received", self, "_on_data")

	var err = client.connect_to_url(websocket_url)
	if err != OK:
		print("Unable to connect")
		set_process(false)
	
func stop():
	client.disconnect_from_host()
	set_process(false)

func _on_data(): 
	var msg = get_msg()
	
#	print("Got data from server: ", msg)
	
	match msg.type:
		"infos":
			id = msg.data.id
			emit_signal("infos_obtained", msg.data)
		"invalid_name":
				emit_signal("invalid_session_joined", msg.data.name)
		"invalid_password":
			emit_signal("invalid_session_password", msg.data.name, msg.data.password)
		"invalid_session_closed":
			emit_signal("invalid_session_closed", msg.data.name)
		"already_registered":
			emit_signal("already_registered_session", msg.data)
		"registered":
			session_name = msg.data.name
			is_server = true
			emit_signal("session_registered", msg.data)
		"joined":
			session_name = msg.data.name
			is_server = msg.data.is_server
			emit_signal("session_joined", msg.data)
		"new_player":
			var peer: WebRTCPeerConnection = WebRTCPeerConnection.new()
			var _i = peer.initialize({
				"iceServers": [ { "urls": ["stun:stun.l.google.com:19302"] } ]
			})
			var _c = peer.connect("session_description_created", self, "_offer_created", [msg.data.id])
			_c = peer.connect("ice_candidate_created", self, "_new_ice_candidate", [msg.data.id])
			
			emit_signal("new_player", msg.data.id, peer)
			
		"start":
			emit_signal("started")
			
			
		"offer":
			emit_signal("offer_received", msg.data.from_id, msg.data.offer)
			
		"answer":
			emit_signal("answer_received", msg.data.from_id, msg.data.answer)
			
		"candidate":
			emit_signal("candidate_received", msg.data.from_id, msg.data.mid_name, msg.data.index_name, msg.data.sdp_name)
			
		"session_closed":
			emit_signal("session_closed")
	
func _new_ice_candidate(mid_name, index_name, sdp_name, candidate_id):
	send_msg("candidate", {
		"mid_name": mid_name,
		"index_name": index_name,
		"sdp_name": sdp_name,
		"target_id": candidate_id
	})

	

func _offer_created(type, data, offer_id):
	emit_signal("offer_created", type, data, id)
	if type == "offer": 
		send_msg("offer", {
			"target_id": offer_id,
			"offer": data
		})
	else:
		send_msg("answer", {
			"target_id": offer_id,
			"answer": data
		})

func _process(_delta):
	client.poll()

func send_msg(type, msg = {}):
	client.get_peer(1).put_packet(JSON.print({"type" : type, "data" : msg}).to_utf8())

func get_msg():
	return JSON.parse(client.get_peer(1).get_packet().get_string_from_utf8()).result
