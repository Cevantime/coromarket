extends TextureRect

var MAX_DRAG_LENGTH = 50
var finger_index = null
var movement = Vector2()

export(Array, String) var left_actions = [ "ui_left" ]
export(Array, String) var right_actions = [ "ui_right" ]
export(Array, String) var down_actions = [ "ui_down" ]
export(Array, String) var up_actions = [ "ui_up" ]

onready var stick = $Stick
onready var initial_offset = stick.rect_position
onready var texture_normal_bak = stick.texture_normal
	
func get_stick_center_pos():
	return stick.rect_global_position + (stick.rect_size * stick.rect_scale) / 2
	
func _process(_delta):
	stick.rect_position = lerp(stick.rect_position, initial_offset + movement, 0.5)
	if finger_index != null:
		stick.texture_normal = stick.texture_pressed
	else :
		stick.texture_normal = texture_normal_bak
	
func _input(event):
	if event is InputEventScreenTouch and get_global_rect().has_point(event.position) and event.is_pressed() and not finger_index :
		finger_index = event.get_index()
		
	elif event is InputEventScreenTouch and not event.is_pressed() and finger_index == event.get_index():
		finger_index = null
		for ar in [right_actions, left_actions, up_actions, down_actions]:
			for name in ar:
				create_axis_event(0, false, name)
				movement = Vector2()
			
	if event is InputEventScreenDrag and finger_index == event.get_index():
		movement = (event.position - get_stick_center_pos()).clamped(MAX_DRAG_LENGTH)
		
		var strength: Vector2 = (movement if movement.length() > 10 else Vector2()).abs()
		for right_action in right_actions:
			create_axis_event(strength.x / MAX_DRAG_LENGTH, movement.x > 0, right_action)
		for left_action in left_actions:
			create_axis_event(strength.x / MAX_DRAG_LENGTH, movement.x < 0, left_action)
		for up_action in up_actions:
			create_axis_event(strength.y / MAX_DRAG_LENGTH, movement.y < 0, up_action)
		for down_action in down_actions:
			create_axis_event(strength.y / MAX_DRAG_LENGTH, movement.y > 0, down_action)
			
			
func create_axis_event(strength, pressed, name):
	var event = InputEventAction.new()
	event.strength = strength
	event.pressed = pressed
	event.action = name
	Input.parse_input_event(event)
	

