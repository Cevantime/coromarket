extends CanvasLayer

signal finished_count()

onready var text = $Text
onready var count_down_player = $CountDownPlayer
onready var tween = $TweenSize
onready var tween_opacity = $TweenOpacity

export(float, 1, 10) var MAX_SIZE = 3.0
export(float, 0.5, 3.5) var COUNT_DURATION = 0.9

onready var interpolated_font_size = MAX_SIZE setget set_interpolated_font_size
onready var interpolated_opacity = 0.5 setget set_interpolated_opacity

func _ready():
	for i in range(1, 10):
		var _s = load("res://assets/sounds/%s.ogg" % i)
	var _l = load("res://assets/sounds/GO.ogg")
	_l = load("res://assets/sounds/GAME OVER.ogg")

func count(nb):
	text.show()
	
	text.text = str(nb)
	tween_opacity.interpolate_property(self, "interpolated_opacity", 0.5, 1, COUNT_DURATION)
	tween.interpolate_property(self, "interpolated_font_size", MAX_SIZE, MAX_SIZE / 15, COUNT_DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	set_interpolated_font_size(MAX_SIZE)
	set_interpolated_opacity(0.5)
	
	if not tween.is_active():
		tween.start()
		
	if not tween_opacity.is_active():
		tween_opacity.start()
	
	count_down_player.stream = load("res://assets/sounds/%s.ogg" % nb)
	
	yield(get_tree().create_timer(0.3),"timeout")
	
	count_down_player.play()
	

func hide():
	text.hide()
	
func set_interpolated_font_size(_size):
	text.rect_scale = Vector2(_size, _size)
	text.rect_pivot_offset = (text.rect_size) / 2
	interpolated_font_size = _size
	
func set_interpolated_opacity(_opacity):
	text.modulate.a = _opacity
	interpolated_opacity = _opacity


func _on_TweenSize_tween_completed(_object, _key):
	emit_signal("finished_count")
