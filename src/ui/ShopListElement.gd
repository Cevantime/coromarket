extends TextureRect

export(Color) var default_outline_color = Color(1.0, 1.0, 1.0)
export(Color) var dirty_outline_color = Color("814f00")
export(Color) var infected_outline_color = Color("ef08db")
export(Color) var color_rupture = Color("ffcb18")
export(Color) var color_promo = Color("8df602")

var product_slot setget set_product_slot
var product setget set_product

func _ready():
	$TextureRect.material = $TextureRect.material.duplicate()

func set_product_slot(p):
	$TextureRect.texture = load(p.icon)
	product_slot = p

func set_product(p):
	product = p
	
	modulate.a = 1.0
	
	var outline_color = default_outline_color
	
	if product.degree == "rupture":
		self_modulate = color_rupture
		outline_color = color_rupture
		
	elif product.degree == "promo":
		self_modulate = color_promo
		outline_color = color_promo
		
	if product.contamined :
		outline_color = infected_outline_color
	
	elif product.dirty:
		outline_color = dirty_outline_color
	
	$TextureRect.material.set_shader_param('outline_color', outline_color)
	
