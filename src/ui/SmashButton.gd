extends TextureButton

export(Array, String) var actions = ["smash"]
	
func create_event(pressed, name):
	var event = InputEventAction.new()
	event.pressed = pressed
	event.action = name
	Input.parse_input_event(event)

func _input(ev):
	if ev is InputEventScreenTouch and get_global_rect().has_point(ev.position):
		for a in actions:
			create_event(ev.pressed, a)
