extends Control

var player_options setget set_player

func set_player(p):
	player_options = p
	$Label.text = p.player_name
