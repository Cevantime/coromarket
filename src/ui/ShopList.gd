tool
extends Panel

export(bool) var fixed_to_bottom = false
var player setget set_player
var packed_list_element_packed = preload("res://src/ui/ShopListElement.tscn")

func set_shopping_kart(shopping_kart):
	for i in range(1, get_child_count()):
		get_child(i).queue_free()
	var products = shopping_kart.products
	for p in products:
		add_list_element(p)
	shopping_kart.connect("product_added", self, "_on_product_added")

func add_list_element(product_infos):
	var shop_list_element = packed_list_element_packed.instance()
	shop_list_element.product_slot = product_infos
	$GridContainer.add_child(shop_list_element)
	# call_deferred("reset_margins")
	return shop_list_element
	
func _process(_delta):
	if fixed_to_bottom:
		margin_top = -rect_size.y

func add_product(product):
	
	for shop_list_element in $GridContainer.get_children():
		if shop_list_element.product == null and shop_list_element.product_slot.name == product.name:
			shop_list_element.product = product
			
			return
	var shop_list_element = add_list_element(product)
	shop_list_element.product = product

func set_player(pl):
	self_modulate = pl.get_albedo()
	for p in pl.shopping_list:
		add_list_element(p)
	pl.connect("assigned_shopping_kart", self, "_on_player_assigned_shopping_kart")

func _on_player_assigned_shopping_kart(pl):
	for product in pl.shopping_kart.products:
		add_product(product)
	set_shopping_kart(pl.shopping_kart)

func _on_GridContainer_item_rect_changed():
	rect_size = $GridContainer.rect_size + Vector2(10, 10)
	$GridContainer.rect_position = Vector2(5, 5)
	
	
func _on_product_added(product_infos):
	add_product(product_infos)
