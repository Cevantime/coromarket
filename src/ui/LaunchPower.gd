extends TextureProgress

export(NodePath) var player_path

var player

func _ready():
	hide()
	if player_path:
		player = get_node(player_path)
		max_value = player.power_max

func _process(delta):
	if ! player:
		return
	
	value = player.power
	
	if value > player.MIN_POWER:
		show()
	else : 
		hide()
