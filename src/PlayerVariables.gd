extends Node

const USER_SETTINGS_PATH = "user://user.settings"
const SCREEN_RESOLUTION_ID = "screen_resolution_id"
const SCREEN_RESOLUTION_X = "screen_resolution_x"
const SCREEN_RESOLUTION_Y = "screen_resolution_y"
const MASTER_VOLUME = "master_volume"
const FX_VOLUME = "fx_volume"
const MUSIC_VOLUME = "music_volume"
const FULLSCREEN = "fullscreen"
const PLAYER_NAME = "player_name"
const PLAYER_CHARACTER = "player_character"

var data

func _ready():
	load_data()
	if not data.has(PLAYER_CHARACTER):
		data[PLAYER_CHARACTER] = "Old Karl"
	OS.window_size = get_screen_resolution()
	OS.window_fullscreen = fullscreen()
	set_bus_volume("Master", get_bus_volume(MASTER_VOLUME))
	set_bus_volume("FX", get_bus_volume(FX_VOLUME))
	set_bus_volume("Music", get_bus_volume(MUSIC_VOLUME))
	
func set_bus_volume(bus_name, db):
	if db is String and db == "-inf":
		db = -INF
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index(bus_name), db)
	
func get_bus_volume(opt):
	var v = get_data(opt)
	if v is String and v == "-inf" :
		v = -INF
	return v if v else 0

func get_user_settings_file() -> File:
	var file = File.new()
	
	if not file.file_exists(USER_SETTINGS_PATH):
		file.open(USER_SETTINGS_PATH, File.WRITE)
		file.store_line("{}")
		file.close()
	
	file.open(USER_SETTINGS_PATH, File.READ_WRITE)
		
	return file
	
func get_screen_resolution():
	
	if get_data(SCREEN_RESOLUTION_ID) != null:
		return Vector2(get_data(SCREEN_RESOLUTION_X), get_data(SCREEN_RESOLUTION_Y))
		
	return Vector2(1040, 600)
	
func fullscreen():
	var fullscreen_data = get_data(FULLSCREEN)
	if fullscreen_data:
		return false if fullscreen_data == "0" else true
	return false
	
func load_data():
	
	var file = get_user_settings_file()
		
	var json = JSON.parse(file.get_as_text())
	
	if json.error :
		
		file.store_line("{}")
		json.result = {}
	
	self.data = json.result
	
	file.close()
	
func get_data(key):
	return data[key] if has_data(key) else null
	
func has_data(key):
	return data.has(key)
	
func set_data(key, value):
	data[key] = value
	
func save_data():
	var file = get_user_settings_file()
	file.store_line(JSON.print(data))
	file.close()
