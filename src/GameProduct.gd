extends Resource

class_name GameProduct

export(PackedScene) var scene
export(int) var min_quantity
export(int) var max_quantity
