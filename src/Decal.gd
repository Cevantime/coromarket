extends Spatial

export(NodePath) var checkout_path

var checkout
var current_body

func _ready():
	if checkout_path:
		checkout = get_node(checkout_path)

func _on_CheckoutArea_body_entered(body):
	if not current_body and ( checkout and body.has_method("groove_to_checkout") ):
		current_body = body
		body.groove_to_checkout(self)
		checkout.character = body.character


func _on_CheckoutArea_body_exited(body):
	if body == current_body:
		current_body = null
