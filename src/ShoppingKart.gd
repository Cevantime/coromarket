extends KinematicBody
	
signal arrived_to_checkout
signal assigned_to_character

export(float) var MAX_SPEED = 10.0
export(float) var MOVE_END_SPEED = 7.5
export(float) var MAX_SPEED_MOBILE = 8.5
export(float) var SPEED_ACCELERATION_MOBILE = 1.0
export(float) var MAX_STEERING = 0.2
export(float) var SPEED_ACCELERATION = 0.025
export(float) var SPEED_BREAKING = 0.01
export(float) var STEERING_ACCELERATION = 0.01
export(float) var STEERING_BREAKING = 0.1
export(float) var GROOVE_SPEED = 0.1
export(bool) var CONTROLLED = false

enum STATES {
	NORMAL,
	GROOVE_TO_CHECKOUT,
	CHECKOUT,
	MOVE_TO_POSITION,
	ROTATE_TO_POSITION
}

var state = STATES.NORMAL
var velocity = Vector3.ZERO
var speed = 0
var speed_mobile = 0
var steering = 0
var original_y
var rotation_y
var character
var character_entered
var products = []
var Character = preload("res://src/Character.gd")
var Product = preload("res://src/Product.gd")
var decal
var target_position

var front_left_colliders = []
var front_right_colliders = []
var back_right_colliders = []
var back_left_colliders = []

signal product_added(product)

func move_to_position(position):
	state = STATES.MOVE_TO_POSITION
	target_position = position

func _ready():
	$Line018.set("material/0", $Line018.get("material/0").duplicate())
	rotation_y = rotation.y
	original_y = transform.origin.y
	
func groove_to_checkout(d):
	if character.has_checked_out or (get_tree().network_peer and not is_network_master()):
		return
	decal = d
	state = STATES.GROOVE_TO_CHECKOUT
	speed = 0
	steering = 0
	if CONTROLLED == false:
		character.take_shopping_kart()

func _physics_process(delta):
	if state == STATES.NORMAL:
		var push = 0
		var steer = 0
		var vertical = 0
		var horizontal = 0
		
		if CONTROLLED:
			push = Input.get_action_strength("push" + character.get_device_str()) - Input.get_action_strength("pull"+ character.get_device_str())
			steer = Input.get_action_strength("steer_left" + character.get_device_str()) - Input.get_action_strength("steer_right" + character.get_device_str())
			vertical = Input.get_action_strength("down_shopping_kart" + character.get_device_str()) - Input.get_action_strength("up_shopping_kart" + character.get_device_str())
			horizontal = Input.get_action_strength("right_shopping_kart" + character.get_device_str()) - Input.get_action_strength("left_shopping_kart" + character.get_device_str())
			
		speed = lerp(speed, MAX_SPEED * push, SPEED_ACCELERATION if push > 0 else SPEED_BREAKING)
		steering = lerp(steering, MAX_STEERING * steer, STEERING_ACCELERATION if abs(steer) > 0.1 else STEERING_BREAKING)
		
		if abs(vertical) > 0 or abs(horizontal) > 0:
			
			velocity.x = horizontal
			velocity.z = vertical
			velocity.y = 0
			
			speed_mobile = lerp(speed_mobile, MAX_SPEED_MOBILE, SPEED_ACCELERATION_MOBILE)
			
			velocity = velocity.normalized() 
			
			var angle = Vector2(velocity.x, velocity.z).angle()
			
			velocity = move_and_slide(velocity * (transform.basis.x.dot(velocity)) * speed_mobile)
				
			if angle :
				var target_transform = Transform(global_transform)
				target_transform.basis = Basis()
				target_transform = target_transform.rotated(Vector3.DOWN, angle)
				
				var quat1 = global_transform.basis.get_rotation_quat()
				var quat2 = target_transform.basis.get_rotation_quat()
				
				var result = quat1.slerp(quat2, 3.0 * delta)
				
				global_transform.basis = Basis(result)
				global_transform = global_transform.orthonormalized()
				
		elif abs(steering) > 0 or abs(speed) > 0:
			velocity = transform.basis.x * speed
			
			velocity = move_and_slide(velocity)
			
			if steering > 0.0:
				if front_left_colliders.empty() or back_right_colliders.empty():
					rotation_y += steering
				else:
					steering = 0
			else:
				if front_right_colliders.empty() or back_left_colliders.empty():
					rotation_y += steering
				else:
					steering = 0
			
			speed = sign(speed) * velocity.length()
			
			transform = Transform(transform)
			transform.basis = Basis()
			rotate_y(rotation_y)
		else: 
			speed_mobile = 0
		
	elif state == STATES.GROOVE_TO_CHECKOUT:
		velocity = Vector3.ZERO
		
		var target = decal.get_node("GroovePoint").global_transform
		target.basis = target.basis.orthonormalized()
		target.origin.y = self.global_transform.origin.y
		global_transform = global_transform.interpolate_with(target, GROOVE_SPEED)
		if target.origin.distance_to(global_transform.origin) < 0.1:
			state = STATES.CHECKOUT
			rotation_y = rotation.y
			emit_signal("arrived_to_checkout")
	
		
	transform.origin.y = original_y
	
func get_character_transform():
	return $TakeArea/CharacterPosition.global_transform

remotesync func assign_character(_character = null, master_id = null):
	if _character:
		character = _character
	else:
		for p in get_tree().get_nodes_in_group("player"):
			if int(p.name) == int(master_id):
				character = p
				break
		set_network_master(master_id)
	
	character.shopping_kart = self
	character.emit_signal("assigned_shopping_kart", character)
	character.get_node("NotifyNetwork").add_node_to_notify(self)
	emit_signal("assigned_to_character", self)
	$Line018.get("material/0").set_shader_param("PlayerColor", character.get_albedo() * 1.1)
	
func taken_by_player():
	$CollisionShapePlayer.disabled = false
	
func leaved_by_player():
	$CollisionShapePlayer.disabled = true
	CONTROLLED = false
	
func _on_Area_body_entered(body):
	if body is Character:
		if (character and body == character) or (not character and not body.shopping_kart):
			character_entered = body
			body.shopping_kart_in_range = self

func _on_Area_body_exited(body):
	if body == character_entered:
		character_entered.shopping_kart_in_range = null
		character_entered = null


func _on_CollectArea_body_entered(product):
	if product is Product and product.is_collectable() :
		add_product(product)

func add_product(product):
	if get_tree().get_network_peer():
		product.rpc("collected")
		rpc("push_product", product.product_infos)
	else:
		product.collected()
		push_product(product.product_infos)
		
remotesync func push_product(product_infos):
	products.push_back(product_infos)
	emit_signal("product_added", product_infos)
	
func back_to_normal():
	state = STATES.NORMAL

func _on_DepositArea_body_entered(body):
	if body is Character :
		body.shopping_karts_in_range_for_deposit.push_front(self)

func _on_DepositArea_body_exited(body):
	if body is Character:
		var karts = body.shopping_karts_in_range_for_deposit
		karts.remove(karts.find(self))

func add_collider(stack, body):
	if body is StaticBody or body is KinematicBody:
		stack.push_back(body)
		
func remove_collider(stack, body):
	if body is StaticBody or body is KinematicBody:
		stack.remove(stack.find(body))

func _on_AreaFrontLeft_body_entered(body):
	add_collider(front_left_colliders, body)


func _on_AreaBackLeft_body_entered(body):
	add_collider(back_left_colliders, body)


func _on_AreaFrontRight_body_entered(body):
	add_collider(front_right_colliders, body)


func _on_AreaBackRight_body_entered(body):
	add_collider(back_right_colliders, body)


func _on_AreaFrontLeft_body_exited(body):
	remove_collider(front_left_colliders, body)


func _on_AreaBackLeft_body_exited(body):
	remove_collider(back_left_colliders, body)


func _on_AreaFrontRight_body_exited(body):
	remove_collider(front_right_colliders, body)


func _on_AreaBackRight_body_exited(body):
	remove_collider(back_right_colliders, body)
