extends "res://src/main_scenes/MainScene.gd"

func _on_ready():
	var idx = 0
	var sorted_players = {}
	
	for id in Lobby.players_options.keys():
		var p_options = Lobby.players_options[id]
		var player = Lobby.build_player_from_options(id, p_options)
		if id == get_tree().get_network_unique_id():
			player.CONTROLLED = true
		
		sorted_players[id] = player
		
	var keys = sorted_players.keys()
	keys.sort()
	for id in keys:
		
		var player = sorted_players[id]
		player.transform = get_node("Characters/PositionPlayer%s" % idx).global_transform
		player.color_edited = idx
		add_child(player)
		player.change_state("Running")
		idx += 1
	
	$GameManager.start_game()
#	Lobby.connect("game_not_ready", self, "_on_Lobby_game_not_ready")
#	Lobby.connect("game_ready", self, "_on_Lobby_game_ready")
#	Lobby.connect("player_added", self, "_on_Lobby_player_added")
	var _c = Lobby.connect("server_disconnected", self, "_on_Lobby_server_disconnected")
	_c = Lobby.connect("server_connected", self, "_on_Lobby_server_connected")
#	Lobby.connect("player_removed", self, "_on_Lobby_player_removed")



func _on_Lobby_player_added(_player):
	pass

func _on_Lobby_server_disconnected():
	$DisconnectedLayer/Panel.show()

func _on_Lobby_server_connected():
	$DisconnectedLayer/Panel.hide()

func _on_BackButton_pressed():
	quit_game()
