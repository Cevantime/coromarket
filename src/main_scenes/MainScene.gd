extends Spatial

var player_options
var is_server = false
var ip_server =""

var player_name_packed = preload("res://src/ui/PlayerName.tscn")

var ShoppingKart = preload("res://src/ShoppingKart.gd")

var musics = [
	preload("res://assets/music/CM_ingame_Part01.ogg"),
	preload("res://assets/music/CM_ingame_Part02_loop.ogg"),
	preload("res://assets/music/CM_ingame_Part03_timesUp.ogg"),
	preload("res://assets/music/CM_ingame_Part04_GameOver.ogg"),
	preload("res://assets/music/CM_highScore.ogg"),
]

func _ready():
	if OS.has_feature('mobile'):
		$MobileLayer/Joystick.show()
		$MobileLayer/SmashButton.show()
	
	#get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_DISABLED, SceneTree.STRETCH_ASPECT_IGNORE, Vector2(1040, 600))
	for p in get_tree().get_nodes_in_group("player"):
		p.change_state("Running")
	$TimerLayer/LabelTimer.text = str($GameManager.game_duration_in_secs)
	for c in get_tree().get_nodes_in_group("confetti"):
		c.hide()
	_on_ready()
		

func _on_ready():
	pass
	
func _process(_delta):
	if Input.is_action_just_pressed("display_in_game_menu"):
		$BackMenuLayer/Control.visible = ! $BackMenuLayer/Control.visible
	
func _on_GameManager_counted_down(time):
	if time > 0:
		$CountDownLayer.count(time)
	else :
		$CountDownLayer.count("GO")
		yield($CountDownLayer, "finished_count")
		$CountDownLayer.hide()
		
func _on_game_finished():
	
	Transition.play_music(musics[3])
	var players = get_tree().get_nodes_in_group("player")
	
	players.sort_custom(self, "sort_player_by_score")
	var i = 0
	
	$CountDownLayer.count("GAME OVER")
	yield($CountDownLayer,"finished_count")
	yield(get_tree().create_timer(1.5), "timeout")
	$CountDownLayer.hide()
	Transition.play_out()
	yield(Transition, "animation_finished")
	for c in get_tree().get_nodes_in_group("confetti"):
		c.show()
	Transition.play_in()
	Transition.play_music(musics[4])
	$SuperMarket/Camera.current = false
	$SuperMarket/EndCamera.current = true
	for p in players:
		i += 1
		var position = get_node("CharactersFinal/PositionPlayer%s" % i)
		p.get_body().get_node("CollisionShape").disabled = true
		if p.shopping_kart:
			p.shopping_kart.call_deferred("queue_free")
			p.shopping_kart = null
		
		p.show()
		p.go_to_end_position(position.global_transform)
	
	$TimerLayer/LabelTimer.hide()
	Transition.play_in()
	yield(Transition, "animation_finished")
	var rank = 1
	for p in players:
		var label_score = Label.new()
		label_score.align = Label.ALIGN_CENTER
		label_score.text = "%s. %s : %s " % [rank, p.player_name, $GameManager.player_scores[p.get_name()] ]
		$ScoreLayer/Control/VBoxContainer.add_child(label_score)
		rank += 1
		
	$ScoreLayer/Control.show()

func _on_game_begun():
	for b in get_tree().get_nodes_in_group("barrier"):
		b.open()
		
func _on_game_started():
	Transition.play_music(musics[0])
	var i = 0
	for player in get_tree().get_nodes_in_group("player"):
		var shop_list = get_node("ListsLayer/Control/ShopList%s" % (i + 1))
		shop_list.player = player
		i += 1
		
	for j in range(i, 4):
		var shop_list = get_node("ListsLayer/Control/ShopList%s" % (j + 1))
		shop_list.hide()
		
func quit_game():
	Transition.transition("res://src/menus/MainMenu.tscn")
	get_tree().network_peer = null
		
func _on_score_updated(_id, _score):
	pass
	
func sort_player_by_score(p1, p2):
	return $GameManager.player_scores[p1.get_name()] >= $GameManager.player_scores[p2.get_name()]
	
func _on_GameManager_game_begun():
	_on_game_begun()

func _on_GameManager_checkout_began():
	Transition.play_music(musics[2])
	for b in get_tree().get_nodes_in_group("barrier_checkout"):
		b.open()
		
		
func _on_GameManager_game_finished():
	_on_game_finished()


func _on_GameManager_game_timeout():
	pass


func _on_GameManager_score_updated(_id, _score):
	pass

func _on_GameManager_timed_down(time):
	var timer = $TimerLayer/LabelTimer
	if time < 0:
		timer.modulate = Color.red
	timer.text = str(time)


func _on_ExitArea_body_entered(body):
	if body is ShoppingKart:
		pass


func _on_BackToMenu_pressed():
	quit_game()


func _on_GameManager_game_started():
	_on_game_started()


func _on_BackButton_pressed():
	quit_game()
