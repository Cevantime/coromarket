extends Camera

export(float, 0.01, 100) var FOV_MUL = 0.00
export(float, 0.01, 100) var Y_MUL = 3.0
export(Vector3) var original_offset = Vector3(0, 7, 5)
export(float) var limit_min_x = 1.0
export(float) var limit_max_z = -1.0

onready var original_fov = fov

func _process(_delta):
	var players = get_tree().get_nodes_in_group("camera_tracked")
	if players.empty():
		return
	
	var maxX = -INF
	var maxZ = -INF
	var minX = INF
	var minZ = INF
	for p in players:
		var p_origin = p.global_transform.origin
		if p_origin.x > maxX:
			maxX = p_origin.x
		if p_origin.x < minX:
			minX = p_origin.x
		if p_origin.z > maxZ:
			maxZ = p_origin.z
		if p_origin.z < minZ:
			minZ = p_origin.z
	
	var pos = Vector3((maxX + minX) / 2, 0, (maxZ + minZ) / 2)
	
	var max_dist = sqrt((maxX - minX) * (maxX - minX) + (maxZ - minZ) * (maxZ - minZ))

	global_transform.origin = lerp(global_transform.origin, pos + original_offset + Vector3(0,1,0) * Y_MUL, 0.1)
	
	if global_transform.origin.x < limit_min_x:
		global_transform.origin.x = limit_min_x
	if global_transform.origin.z > limit_max_z:
		global_transform.origin.z = limit_max_z
		
	fov = lerp(fov, original_fov + FOV_MUL * max_dist, 0.1)
