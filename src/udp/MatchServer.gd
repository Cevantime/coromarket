extends Node

class GameSession:
	var name
	var server_id
	var server_ip
	var server_internal_ip
	var server_port
	var password
	var members = {}
	var created_time
	
	func _init():
		created_time = OS.get_system_time_secs()
		
	func must_be_closed():
		return OS.get_system_time_secs() - created_time > 300
	
	func infos(local = false):
		return {
			"name": name, 
			"server_ip": server_internal_ip if local else server_ip, 
			"server_id": server_id,
			"server_port": server_port,
			"password": password,
			"members": members.size()
		}

class SessionMember:
	var ip
	var internal_ip
	var port
	var id
	
class OpenedPort:
	var packet
	var ref_count = 1
	
const MATCH_PORT = 54322
const MATCH_IP = "51.255.36.188"
#const MATCH_IP = "localhost"
#const MATCH_IP = "192.168.1.24"
var udp_packet = PacketPeerUDP.new()
var udp_packet_pool = {}
var udp = preload("res://src/udp/UDPHelper.gd").new()

var sessions = {}

func _ready():
	udp_packet.listen(MATCH_PORT)
	var timer_check_session_expired = Timer.new()
	timer_check_session_expired.wait_time = 10.0
	add_child(timer_check_session_expired)
	timer_check_session_expired.connect("timeout", self, "_on_timer_check_session_expired_timeout")
	timer_check_session_expired.start()
	
func get_a_packet(port):
	if udp_packet_pool.has(port):
		udp_packet_pool[port].ref_count += 1
		return udp_packet_pool[port]
	
	var op = OpenedPort.new()
	var p = PacketPeerUDP.new()
	p.listen(port)
	op.packet = p
	udp_packet_pool[port] = op
	
	
func _process(_delta):
	var packet_count = udp_packet.get_available_packet_count()
	for i in packet_count:
		
		var packet = udp.receive_packet(udp_packet)
		
		if not(packet is udp.PacketInfo):
			continue
		
		udp_packet.set_dest_address(packet.ip, packet.port)
		
		match packet.type:
			"get_infos":
				var sessions_infos = []
				for s in sessions.values():
					if s.members.size() < 4:
						sessions_infos.push_back(s.infos())
				udp.send_packet(udp_packet, "infos", {
					"ip": packet.ip, 
					"port": packet.port,
					"sessions" : sessions_infos
				})
				
			"register":
				var session_name = packet.data.name
				if sessions.has(session_name) :
					var session = sessions[session_name]
					udp.send_packet(udp_packet, "already_registered", session.infos(packet.ip == session.server_ip))
				else :
					var session = GameSession.new()
					session.server_ip = packet.ip
					session.server_port = packet.port
					session.server_internal_ip = packet.data.internal_ip
					session.name = session_name
					session.server_id = packet.data.id
					if packet.data.has('password'):
						session.password = packet.data.password
					else:
						session.password = ""
					sessions[session_name] = session
					var member = SessionMember.new()
					member.ip = packet.ip
					member.id = packet.data.id
					member.internal_ip = packet.data.internal_ip
					member.port = packet.port
					session.members[member.id] = member
					udp.send_packet(udp_packet, "registered", session.infos(packet.ip == session.server_ip))
				
			"join":
				var session_name = packet.data.name
				if not (sessions.has(session_name)) :
					udp.send_packet(udp_packet, "invalid_name", {"name":session_name})
				elif sessions[session_name].password != "" and sessions[session_name].password != null and (not packet.data.has('password') or packet.data.password != sessions[session_name].password):
					udp.send_packet(udp_packet, "invalid_password", {"name":session_name, "password": packet.data.password})
				else:
					var session = sessions[session_name]
					var session_infos = session.infos(packet.ip == session.server_ip)
					session_infos.is_server = session.server_id == packet.data.id
					udp.send_packet(udp_packet, "joined", session_infos)
					
					for k in session.members:
						var m = session.members[k]
						udp_packet.set_dest_address(m.ip, m.port)
#						get_a_packet(packet.port)
						udp.send_packet(udp_packet, "new_user", {
							"ip": packet.ip if m.ip != packet.ip else packet.data.internal_ip, 
							"id": packet.data.id,
							"port": packet.port
						})
						udp_packet.set_dest_address(packet.ip, packet.port)
#						get_a_packet(m.port)
						udp.send_packet(udp_packet, "new_user", {
							"ip": m.ip if m.ip != packet.ip else m.internal_ip, 
							"id": m.id,
							"port": m.port
						})
						
					var member = SessionMember.new()
					member.ip = packet.ip
					member.id = packet.data.id
					member.internal_ip = packet.data.internal_ip
					member.port = packet.port
					session.members[member.id] = member
			
			"close":
				if not (packet.data.name in sessions):
					udp.send_packet(udp_packet, "invalid_session_closed", {"name": packet.data.name})
				else :
					sessions.erase(packet.data.name)
			
		
#	for port in udp_packet_pool:
#		var op = udp_packet_pool[port]
#		var p = op.packet
#		var p_count = p.get_available_packet_count()
#		if p_count > 0:
#			op.ref_count -= 1
#			if op.ref_count <= 0:
#				udp_packet_pool.erase(port)
#
#		for _i in range(p_count):
#			var packet = udp.receive_packet(p)
#			if not(packet is udp.PacketInfo):
#				continue
#
#			p.set_dest_address(packet.ip, packet.port)
#			udp.send_packet(p, "pong", packet.data)
	
func _on_timer_check_session_expired_timeout():
	for k in sessions:
		var session = sessions[k]
		if session.must_be_closed():
			for m in session.members.values():
				udp_packet.set_dest_address(m.ip, m.port)
				udp.send_packet(udp_packet, "session_closed", {'name': session.name})
			sessions.erase(k)
