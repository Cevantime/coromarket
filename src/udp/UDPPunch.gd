extends Node

class HandCheck:
	var player_options
	var port
	var ip
	var id

	
signal infos_obtained(infos)
signal session_joined()
#signal server_infos_obtained(infos)
signal invalid_session_closed(name)
signal invalidsession_joined(name)
signal invalid_session_password(name, password)
signal already_registered_session(infos)
signal session_registered(infos)
signal hand_checked(p_options)
signal player_joined(p_options)
signal player_lost(p_potions)
signal started()

var MatchServer = preload("res://src/udp/MatchServer.gd")
var udp = preload("res://src/udp/UDPHelper.gd").new()
var own_port
var udp_packet = PacketPeerUDP.new()
var server_ip
var server_port
var is_server = false
var session_name
var received_hand_checks = {}
var checks_history = {}
var player_options
var timer_check_hands
var id = UUID.v4()
var internal_port = 61000

func _ready():
	timer_check_hands = Timer.new()
	add_child(timer_check_hands)
	timer_check_hands.wait_time = 2.0
	timer_check_hands.connect("timeout", self, "_on_timer_check_hands")

func start():
	timer_check_hands.start()
	set_process(true)
	
func stop():
	timer_check_hands.stop()
	set_process(false)
	udp_packet.close()
	
func _on_timer_check_hands():
	for ip in received_hand_checks:
		if received_hand_checks[ip] == null and checks_history[ip].player_options != null:
			emit_signal("player_lost", checks_history[ip])
			received_hand_checks.erase(ip)
			
	for u_id in checks_history:
		var hc = checks_history[u_id]
		
		udp_packet.set_dest_address(hc.ip, hc.port)
		
		if received_hand_checks.has(u_id):
			received_hand_checks[u_id] = null
			
		print("send check hand to %s:%s" % [hc.ip, hc.port])
		
		udp.send_packet(udp_packet, "check_hand", {
			"received_hand_check": false,
			"player_options": player_options,
			"own_port": hc.port,
			"id": id
		})
		
func disconnect_player(id):
	if OS.has_feature("debug"):
		print("player lost %s", id)
	emit_signal("player_lost", checks_history[id])
	received_hand_checks.erase(id)
	
func leave_session():
	received_hand_checks = {}
	checks_history = {}
	player_options = null
	
	for hc in checks_history.values():
		udp_packet.set_dest_address(hc.ip, hc.port)
		udp.send_packet(udp_packet, "leave", {"id" :id})
	
	stop()
		
func close_session():
	if not session_name:
		return
	udp_packet.set_dest_address(MatchServer.MATCH_IP, MatchServer.MATCH_PORT)
	udp.send_packet(udp_packet, "close", {"name": session_name })
	var hand_checks = received_hand_checks
	for id in hand_checks:
		var hc = checks_history[id]
		udp_packet.set_dest_address(hc.ip, hc.port)
		udp.send_packet(udp_packet, "start", {"own_port" : hc.port})
	stop()
	
func register_session(p_options, name, password):
	create_or_join(p_options, name, password, "register")
	
func join_session(p_options, name, password):
	create_or_join(p_options, name, password, "join")
	
func get_infos():
	set_process(true)
	udp_packet.set_dest_address(MatchServer.MATCH_IP, MatchServer.MATCH_PORT)
	udp.send_packet(udp_packet, "get_infos")
	
func create_or_join(p_options, name, password, verb = "register"):
	start()
	player_options = p_options
	if not udp_packet.is_listening():
		udp_packet.listen(internal_port)
	udp_packet.set_dest_address(MatchServer.MATCH_IP, MatchServer.MATCH_PORT)
	udp.send_packet(udp_packet, verb, { 
		"name": name, 
		"password": password, 
		"internal_ip" : get_internal_ip(),
		"id": id
	})

func get_internal_ip():
	var ip_addresses = IP.get_local_addresses()
	
	for ip in ip_addresses:
		if ip.substr(0, 8) == "192.168.":
			return ip
			
	return ip_addresses[0]
	
func _process(_delta):
	var udp_packet_count = udp_packet.get_available_packet_count()
	
	for _i in range(udp_packet_count):
		
		var packet = udp.receive_packet(udp_packet)
		
		if not(packet is udp.PacketInfo):
			continue
		
		match packet.type:
			"infos":
				emit_signal("infos_obtained", packet.data)
				server_ip = packet.data.ip
				server_port = packet.data.port
			"invalid_name":
				emit_signal("invalidsession_joined", packet.data.name)
			"invalid_password":
				emit_signal("invalid_session_password", packet.data.name, packet.data.password)
			"invalid_session_closed":
				emit_signal("invalid_session_closed", packet.data.name)
			"already_registered":
				emit_signal("already_registered_session", packet.data)
			"registered":
				server_ip = packet.data.server_ip
				server_port = packet.data.server_port
				session_name = packet.data.name
				is_server = true
				emit_signal("session_registered", packet.data)
			"joined":
				server_ip = packet.data.server_ip
				server_port = packet.data.server_port
				session_name = packet.data.name
				is_server = packet.data.is_server
				emit_signal("session_joined", packet.data)
			"new_user":
#				udp_packet.set_dest_address(MatchServer.MATCH_IP, int(packet.data.port))
#				udp.send_packet(udp_packet, "ping", {"next_ip" : packet.data.ip, "next_port": packet.data.port, "next_id": packet.data.id})
				udp_packet.set_dest_address(packet.data.ip, int(packet.data.port))
				udp.send_packet(udp_packet, "check_hand", {
					"received_hand_check": received_hand_checks.has("%s:%s" % [ packet.data.ip, packet.data.port ]),
					"player_options": player_options,
					"own_port": packet.data.port,
					"id": id
				})
				if packet.data.id != id:
					var hand_check = HandCheck.new()
					hand_check.port = packet.data.port
					hand_check.ip = packet.data.ip
					checks_history[packet.data.id] = hand_check
				
#			"pong": 
#				udp_packet.set_dest_address(packet.data.next_ip, int(packet.data.next_port))
##				udp_packet.listen(MatchServer.MATCH_PORT)
#				udp.send_packet(udp_packet, "check_hand", {
#					"received_hand_check": received_hand_checks.has("%s:%s" % [ packet.data.next_ip, packet.data.next_port ]),
#					"player_options": player_options,
#					"own_port": packet.data.next_port,
#					"id": id
#				})
#				var hand_check = HandCheck.new()
#				hand_check.port = packet.data.next_port
#				hand_check.ip = packet.data.next_ip
#				checks_history[packet.data.next_id] = hand_check
			
			"check_hand":
				var hand_check = HandCheck.new()
				hand_check.port = packet.port
				hand_check.ip = packet.ip
				hand_check.id = packet.data.id
				hand_check.player_options = packet.data.player_options
				if not received_hand_checks.has(packet.data.id) :
					emit_signal("player_joined", hand_check)
				
				received_hand_checks[packet.data.id] = hand_check
				checks_history[packet.data.id] = hand_check

				if not packet.data.received_hand_check: 
					udp_packet.set_dest_address(packet.ip, int(packet.port))
					udp.send_packet(udp_packet, "check_hand",  {
						"received_hand_check": true,
						"player_options": player_options,
						"own_port": packet.port,
						"id": id
					})
				if packet.data.own_port:
					own_port = packet.data.own_port
				
				emit_signal("hand_checked", hand_check)
				
			"start":
				own_port = packet.data.own_port
				stop()
				emit_signal("started")
			
			"session_closed":
				if packet.data.name == session_name:
					stop()
					emit_signal("started")
				
			"leave":
				var id = packet.data.id
				if checks_history.has(id):
					disconnect_player(id)
					received_hand_checks.erase(id)
					
		
				
