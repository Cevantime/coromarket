extends Node

# var dictionnary = {
# 	'J': {
# 		"type" : "Join",
# 		"segments" : {
# 			'person:name' : 'string',
# 			'person:age' : 'integer',
# 			'id': 'integer'
# 		}
# 	}
# }

class PacketInfo:
	var data
	var type
	var ip
	var port

func send_packet(udp_packet, type, data = {}):
	var packet = PoolByteArray()
	var msg = JSON.print({"type": type, "data": data})
	packet.append_array(msg.to_utf8())
	if OS.has_feature('debug'):
		print("packet sent : ", msg)
		
	udp_packet.put_packet(packet)
	
func receive_packet(udp_packet):
	var packet_received = udp_packet.get_packet()
	var packet_content = packet_received.get_string_from_utf8()
	print(packet_content.length())
	# TODO check JSON integrity
	var parsed = JSON.parse(packet_content)
	if parsed.error != OK:
		if OS.has_feature('debug'):
			print("invalid packet received : ", packet_content)
		return packet_content
	var packet_json = parsed
	var packet_info = PacketInfo.new()
	packet_info.data = packet_json.data
	packet_info.type = packet_json.type
	packet_info.ip = udp_packet.get_packet_ip()
	packet_info.port = udp_packet.get_packet_port()
	if OS.has_feature('debug'):
		print("packet received ! ")
		print("packet ip ", packet_info.ip)
		print("packet port ", packet_info.port)
		print("packet type ", packet_info.type)
		print("packet data ", packet_info.data)
	return packet_info
	
func receive_raw(udp_packet):
	var packet_received = udp_packet.get_packet()
	return packet_received.get_string_from_utf8()
	

#func parse_str(msg):
#
#	if msg.empty():
#		printerr("Packet parsing error, received empty string")
#		return 
#
#	var segments = msg.split(':')
#
#	var t = segments[0]
#
#	if not dictionnary.has(t):
#		printerr("Packet parsing error, unexistent type : %s" % t) 
#		return false
#
#	segments.remove(0)
#
#	var infos = dictionnary[t]
#
#	if not(infos.has('type')) or not(infos.has('segments')):
#		printerr("Packet parsing error, not type or no sgements specified" % t) 
#		return false
#
#	var parsed = PacketInfo.new()
#
#	parsed.type = infos['type']
#
#	var data = {}
#	var conf_segs = infos['segments']
#
#	if conf_segs.size() > segments.size():
#		printerr("Packet parsing error : there are more expected keys than in the actual packet")
#
#	var i = 0
#
#	for s in conf_segs.keys():
#		fill_value(data, s, segments[i], conf_segs[s])
#		i += 1
#
#	parsed.data = data
#
#	return parsed
#
#func fill_value(data, key, value, value_type):
#	var k_segs = key.split(':')
#
#	if k_segs.size() == 1:
#		var v = value
#		if not v:
#			return
#		if value_type == 'integer':
#			v = int(v)
#		elif value_type == 'float':
#			v = float(v)
#		elif value_type == 'bool':
#			v = false if v == 'False' else true
#
#		data[key] = v
#		return
#
#	var first = k_segs[0]
#
#	k_segs.remove(0)
#
#	if not data.has(first):
#		data[first] = {}
#
#	fill_value(data[first], k_segs.join(':'), value, value_type)
#
#func serialize(msg):
#	if not msg.has('type'):
#		printerr('Serializing message error : message has no type.')
#		return false
#
#	var type = null
#	for k in dictionnary.keys():
#		if(dictionnary[k].type == msg.type):
#			type = k
#			break
#
#	if not type:
#		printerr('Serializing message error : unexisting message type : %s.' % msg.type)
#		return false
#
#	var infos = dictionnary[type]
#
#	if not infos.has('segments'):
#		printerr('Serializing message error : no configured segment for type : %s.' % type)
#		return false
#
#	var result = type
#
#	var segs = infos['segments']
#
#	for s in segs:
#		result += ':' + str(get_value(s, msg.data))
#
#	return result
#
#func get_value(key, data, current_object = ''):
#	var k_segs = key.split(':')
#
#	var k = k_segs[0]
#
#	if k_segs.size() == 1:
#		if not data.has(k):
#			printerr('Serializing message error : unexisting key %s in object %s' % [k, current_object if current_object else 'global object'])
#			return false
#		return data[k]
#
#	var d = data[k]
#
#	if not (d is Dictionary):
#		printerr('Serializing message error : key %s in object %s should be a dictionnary' % [k, current_object if current_object else 'global object'])
#
#	k_segs.remove(0)
#
#	return get_value(k_segs.join(':'), data[k], current_object + ':' + k)
