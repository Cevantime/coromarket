extends Control

var game_packed_scene = preload("res://tests/thibault/TestNetworked.tscn")

func _on_JoinGameButton_pressed():
	var ip = $Panel/VBoxContainer/IpLineEdit.text
	change_to_game(false, ip)

func _on_NewGameButton_pressed():
	change_to_game(true)

func change_to_game(server, ip = null):
	var game_scene = game_packed_scene.instance()
	
	game_scene.is_server = server
	game_scene.server_ip = ip
	
	var root = get_node("/root")
	root.get_child(root.get_child_count() - 1).queue_free()
	
	root.add_child(game_scene)
