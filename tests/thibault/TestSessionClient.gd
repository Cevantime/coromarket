extends Node

func _ready():
	
	$UDPPunch.join_session({}, "Test")
	
	yield($UDPPunch, "started")
	
	var peer = NetworkedMultiplayerENet.new()
	
	get_tree().set_network_peer(peer)

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
func _player_connected(id):
	
	
func _player_disconnected(id):
	

func _connected_ok():
	

func _server_disconnected():
	

func _connected_fail():
	
