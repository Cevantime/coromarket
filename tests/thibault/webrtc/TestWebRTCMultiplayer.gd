extends Node


onready var session_list = $Control/VBoxContainer/ScrollContainer/SessionList
onready var client = $WebRTCClient
onready var session_name_edit = $Control/VBoxContainer/LineEdit

var rtc_mp = WebRTCMultiplayer.new()
var id 
var server_id

func _process(delta):
	rtc_mp.poll()
	
	while rtc_mp.get_available_packet_count() > 0:
		print(rtc_mp.get_packet().get_string_from_utf8())

func _on_WebRTCClient_infos_obtained(infos):
	print("infos obtained : %s" % infos)
	id = infos.id
	for s in infos.sessions:
		var button = Button.new()
		button.text = s.name
		session_list.add_child(button)
		button.connect("pressed", self, "_on_button_session_pressed", [s.name])


func _on_button_session_pressed(name):
	client.join_session({}, name, '')


func _on_Button_pressed():
	client.register_session({}, session_name_edit.text, "")


func _on_WebRTCClient_session_registered(infos):
	rtc_mp.initialize(1, true)
	server_id = infos.server_id
	connect_tree()

func connect_tree():
	get_tree().network_peer = rtc_mp
	if not get_tree().is_connected("network_peer_connected", self, "_player_connected"):
		var _c = get_tree().connect("network_peer_connected", self, "_player_connected")
		_c = get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		_c = get_tree().connect("connected_to_server", self, "_connected_ok")
		_c = get_tree().connect("connection_failed", self, "_connected_fail")
		_c = get_tree().connect("server_disconnected", self, "_server_disconnected")


func _on_WebRTCClient_session_joined(infos):
	rtc_mp.initialize(1 if infos.is_server else id, true)
	server_id = infos.server_id
	connect_tree()



func _on_WebRTCClient_new_player(id, peer):
	rtc_mp.add_peer(peer, mp_id(id))
	if mp_id(id) > rtc_mp.get_unique_id():
		print("create offer")
		peer.create_offer()

	
func _player_connected(id):
	
	print("player connected %s" % id)
	
func _player_disconnected(id):
	print("player disconnected %s" % id)

func _connected_ok():
	print("connected to server")
	get_tree().network_peer = rtc_mp

func _server_disconnected():
	print('disconnected from server')

func _connected_fail():
	get_tree().set_network_peer(null)

func mp_id(id):
	return 1 if id == server_id else id

func _on_WebRTCClient_offer_received(id, offer):
	if rtc_mp.has_peer(mp_id(id)):
		rtc_mp.get_peer(mp_id(id)).connection.set_remote_description("offer", offer)


func _on_WebRTCClient_answer_received(id, answer):
	if rtc_mp.has_peer(mp_id(id)):
		rtc_mp.get_peer(mp_id(id)).connection.set_remote_description("answer", answer)


func _on_WebRTCClient_candidate_received(id, mid, index, sdp):
	if rtc_mp.has_peer(mp_id(id)):
		rtc_mp.get_peer(mp_id(id)).connection.add_ice_candidate(mid, index, sdp)


func _on_WebRTCClient_offer_created(type, data, id):
	if rtc_mp.has_peer(mp_id(id)):
		rtc_mp.get_peer(mp_id(id)).connection.set_local_description(type, data)
