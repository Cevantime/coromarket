extends Spatial

var is_server = false

var server_ip = '127.0.0.1'
const SERVER_PORT = 10000
const SERVER_PUBLIC_PORT = 10000
const MAX_PLAYER = 2

var character_packed = preload("res://src/characters/GangstaCharacter.tscn")
var player
var player_ready = 0

func _ready():
	randomize()
	
	var upnp = UPNP.new()
	upnp.discover()
	if upnp.get_gateway() and upnp.get_gateway().is_valid_gateway():
		upnp.add_port_mapping(SERVER_PUBLIC_PORT, SERVER_PORT, "Godot", "UDP")
	
	if is_server:
		var peer = NetworkedMultiplayerENet.new()
		peer.create_server(SERVER_PORT, 4)
		get_tree().set_network_peer(peer)
		player = create_player(get_tree().get_network_unique_id())
		
	else :
		var peer = NetworkedMultiplayerENet.new()
		peer.create_client(server_ip, SERVER_PUBLIC_PORT)
		get_tree().set_network_peer(peer)
		
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	get_tree().paused = true
	
func _player_connected(id):
	
	if !player:
		player = create_player(get_tree().get_network_unique_id())
	
	rpc_id(id, "add_new_player", get_tree().get_network_unique_id())
	
func _player_disconnected(id):
	pass

func _connected_ok():
	if !player:
		player = create_player(get_tree().get_network_unique_id())

func _server_disconnected():
	get_tree().change_scene("res://tests/thibault/Disconnected.tscn")
	get_tree().set_network_peer(null)

func _connected_fail():
	pass

func create_player(id):
	var new_player = character_packed.instance()
	new_player.set_name(str(id))
	new_player.set_network_master(id)
	new_player.CONTROLLED = id == get_tree().get_network_unique_id()
	if new_player.CONTROLLED :
		new_player.connect("assigned_shopping_kart", self, "_on_character_took_shopping_kart")
	Lobby.players[id] = new_player
	if Lobby.players.values().size() == MAX_PLAYER:
		rpc_id(1, "add_player_ready")
	return new_player

remotesync func add_player_ready():
	player_ready += 1
	
	if player_ready == MAX_PLAYER:
		rpc("start")
	
remotesync func start():
	var idx = 1
	var keys = Lobby.players.keys()
	keys.sort()
	
	for id in keys:
		var player = Lobby.players[id]
		player.transform = get_node("PositionPlayer%s" % idx).global_transform
		player.color_edited = idx - 1
		add_child(player)
		idx += 1
	
	get_tree().paused = false

remote func add_new_player(id):
	create_player(id)

func _on_character_took_shopping_kart(character):
	$CanvasLayer/Control/LaunchPower.player = character
	$CanvasLayer/Control/Control/ShopList.shopping_kart = character.shopping_kart
