extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$ViewportContainer2/Viewport.world = $ViewportContainer/Viewport.world


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ViewportContainer2/Viewport/Camera.global_transform = $ViewportContainer/Viewport/Main/Character/Camera.global_transform
