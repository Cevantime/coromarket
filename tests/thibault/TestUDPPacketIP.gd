extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var u = preload("res://src/udp/UDPHelper.gd").new()
	var p = PacketPeerUDP.new()
	p.listen(54321)
	p.set_dest_address("51.255.36.188", 54322)
	u.send_packet(p, "test")
	print("ip ", p.get_packet_ip())
	print("port ", p.get_packet_port())
	
	p.set_dest_address("92.184.104.23", 12345)
	u.send_packet(p, "test")
	print("ip ", p.get_packet_ip())
	print("port ", p.get_packet_port())

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
