extends Control

var game_packed_scene = preload("res://src/main_scenes/NetworkedScene.tscn")
var items = {}

func _ready():
	var i = 0
	for c in Lobby.character_packed:
		$VBoxContainer/CharacterOption.add_item(c)
		items[i] = c
		i += 1

func _on_JoinGameButton_pressed():
	var player_name = $VBoxContainer/NameLineEdit.text
	if ! player_name:
		return
	var ip = $VBoxContainer/IpLineEdit.text
	change_to_game(false, { "player_name": player_name, "character" : items[$VBoxContainer/CharacterOption.selected] }, ip)

func _on_NewGameButton_pressed():
	var player_name =  $VBoxContainer/NameLineEdit.text
	if ! player_name:
		return
	change_to_game(true, { "player_name": player_name, "character" : items[$VBoxContainer/CharacterOption.selected] })

func change_to_game(server, player_options, ip = null):
	var game_scene = game_packed_scene.instance()
	
	game_scene.player_options = player_options
	
	game_scene.is_server = server
	game_scene.ip_server = ip
	
	var root = get_node("/root")
	root.get_child(root.get_child_count() - 1).queue_free()
	
	root.add_child(game_scene)
